/* eslint-disable no-extra-label */

import { ClusterSquare, StringDict, Position } from '../store/types'

interface Rectangle {
    left: number
    top: number
    width: number
    height: number
}

interface ScreenParams {
    width: number
    height: number
    clusterWidth: number
    clusterHeigth: number
}

/**
 * Calculates positions of the cluster from the given precomputed positions from backend.
 * 
 * @param width Graph space width
 * @param height Graph space height
 * @param clusterWidth Width of a cluster
 * @param clusterHeigth Width of a cluster
 * @param clusterSquares Cluster squares as given from the backend
 * 
 * @returns Dictionary of positions: [clusterId]:Position
 */
export const computeClusterPositions =
    (width: number, height: number, clusterWidth: number, clusterHeigth: number, clusterSquares: ClusterSquare[]):
        StringDict<Position> => {
        const toRet: StringDict<Position> = {} // prepare dict for final positions
        width -= 15 // some distance from the edge

        const oneClusterComponents = []
        const twoClusterComponents = []
        const threeClusterComponents = []
        const bigComponent = []

        for (let i = 0; i < clusterSquares.length; i++) {
            const square = clusterSquares[i];

            const clustersInSquares = Object.keys(square).length
            if (clustersInSquares === 1) {
                oneClusterComponents.push(square)
            }
            else if (clustersInSquares === 2) {
                twoClusterComponents.push(square)
            }
            else if (clustersInSquares === 3) {
                threeClusterComponents.push(square)
            }
            else {
                bigComponent.push(square)
            }
        }

        const clusterPadding = 25
        const screenParams: ScreenParams = { width: width, height: height, clusterWidth: clusterWidth + clusterPadding, clusterHeigth: clusterHeigth + 10 }
        getBigComponentsPositions(bigComponent, toRet, screenParams)
        const currentHeight = computeHeigthFromPositions(toRet, clusterHeigth)
        getRestComponentsPositions(toRet, oneClusterComponents, twoClusterComponents, threeClusterComponents, screenParams, currentHeight)
        return toRet
    }

/**
 * The big components (bigger than 3 clusters) are allowed to occupy the main space.
 * @param bigClusterSquares ClusterSquares as received from the backend with more than 3 clusters
 * @param finalPositions Dictionary with final positions to be changed! 
 * @param screenParams Screen parameters
 */
const getBigComponentsPositions = (
    bigClusterSquares: ClusterSquare[],
    finalPositions: StringDict<Position>,
    screenParams: ScreenParams
) => {
    for (let i = 0; i < bigClusterSquares.length; i++) {
        const square = bigClusterSquares[i];
        for (let clusterId in square) {
            const clusterPosition = square[clusterId]

            const newX = clusterPosition.x * (screenParams.width - screenParams.clusterWidth - 40)
            const newY = clusterPosition.y * screenParams.height
            finalPositions[clusterId] = { x: newX, y: newY }
        }
    }
}

/**
 * Fills free space that was not occupied by big components yet
 * 
 * @param clusterInComponent Number of clusters in the components (1/2/3)
 * @param clusterComponents list of components with `clusterInComponent` clusters to be places
 * @param freeRectMatrix Binary matrix - 1 free space, 0 already filled space
 * @param possibleRectanglePositions Rectangles that are empty
 * @param notUsedClusterComponents Components that were not places yes -> will be changed
 * @param finalPositions Dictionary with final positions to be changed! 
 */
const tryFillFreeSpace = (
    clusterInComponent: number,
    clusterComponents: ClusterSquare[],
    freeRectMatrix: number[][],
    possibleRectanglePositions: Rectangle[],
    notUsedClusterComponents: ClusterSquare[],
    finalPositions: StringDict<Position>,
) => {
    if (clusterInComponent !== 1 && clusterInComponent !== 2 && clusterInComponent !== 3 && freeRectMatrix.length <= 0) {
    }

    const freeRectMatrixRows = freeRectMatrix.length
    const freeRectMatrixCols = freeRectMatrix[0].length

    wholeLoop:
    for (let i = 0; i < clusterComponents.length; i++) {
        let foundSomeCoords = false
        const clusterComponent = clusterComponents[i];

        rowLoop:
        for (let row = 0; row < freeRectMatrix.length; row++) {

            // columnLoop:
            for (let col = 0; col < freeRectMatrix[row].length; col++) {

                let coordCombinations: number[][][] = []
                if (clusterInComponent === 1) {
                    coordCombinations = [
                        [[row, col]]
                    ]
                }
                else if (clusterInComponent === 2) {
                    coordCombinations = [
                        [[row, col], [row, col + 1]],
                        [[row, col], [row + 1, col]],
                    ]
                }
                else if (clusterInComponent === 3) {
                    coordCombinations = [
                        [[row, col], [row, col + 1], [row + 1, col + 1]],
                        [[row, col], [row + 1, col], [row + 1, col + 1]],
                        [[row, col], [row, col + 1], [row + 1, col]],
                        [[row + 1, col + 1], [row, col + 1], [row + 1, col]],
                    ]
                }

                for (let coordIndex = 0; coordIndex < coordCombinations.length; coordIndex++) {
                    const coordArray = coordCombinations[coordIndex]
                    const isLegit = coordArray.every(([rIndex, cIndex]) => {
                        return rIndex < freeRectMatrixRows && cIndex < freeRectMatrixCols && freeRectMatrix[rIndex][cIndex] !== 0
                    })
                    const clusterIds = Object.keys(clusterComponent);
                    if (isLegit) {

                        for (let k = 0; k < clusterInComponent; k++) {
                            const rect = possibleRectanglePositions[freeRectMatrix[coordArray[k][0]][coordArray[k][1]] - 1]
                            finalPositions[clusterIds[k]] = { x: rect.left, y: rect.top }
                            freeRectMatrix[coordArray[k][0]][coordArray[k][1]] = 0
                        }

                        foundSomeCoords = true
                        break rowLoop
                    }
                    else { }
                }
            }
        }

        if (!foundSomeCoords) {
            for (let notUsedIndex = i; notUsedIndex < clusterComponents.length; notUsedIndex++) {
                notUsedClusterComponents.push(clusterComponents[notUsedIndex])
            }
            break wholeLoop
        }
    }
}

/**
 * Places all small components.
 * 
 * @param finalPositions Dictionary with final positions to be changed! 
 * @param oneClusterComponents Components with one cluster
 * @param twoClusterComponents Components with two clusters
 * @param threeClusterComponents Components with three clusters
 * @param screenParams Parameters of the screen
 * @param currentHeight Current height after big components were already places
 */
const getRestComponentsPositions = (
    finalPositions: StringDict<Position>,
    oneClusterComponents: ClusterSquare[],
    twoClusterComponents: ClusterSquare[],
    threeClusterComponents: ClusterSquare[],
    screenParams: ScreenParams,
    currentHeight: number
) => {
    // get the empty rectangle to fill with small pieces..

    let rectangles: Rectangle[] = []
    for (let key in finalPositions) {
        let pos = finalPositions[key]
        rectangles.push({ left: pos.x, top: pos.y, width: screenParams.clusterWidth, height: screenParams.clusterHeigth })
    }
    const { possibleRectanglePositions, freeRectMatrix } = getPossibleFreePositions(
        screenParams.width, currentHeight, screenParams.clusterWidth + 30, screenParams.clusterHeigth + 30, rectangles);

    const notUsedThreeClusterComponents: ClusterSquare[] = []
    const notUsedTwoClusterComponents: ClusterSquare[] = []
    const notUsedOneClusterComponents: ClusterSquare[] = []

    tryFillFreeSpace(3, threeClusterComponents, freeRectMatrix, possibleRectanglePositions, notUsedThreeClusterComponents, finalPositions)
    tryFillFreeSpace(2, twoClusterComponents, freeRectMatrix, possibleRectanglePositions, notUsedTwoClusterComponents, finalPositions)
    tryFillFreeSpace(1, oneClusterComponents, freeRectMatrix, possibleRectanglePositions, notUsedOneClusterComponents, finalPositions)

    const newRects: Rectangle[] = []

    let currentY = currentHeight
    let currentX = 0
    
    /* THREE CLUSTERS COMPONENTS */
    const middleX = screenParams.clusterWidth * 0.7
    const middleY = screenParams.clusterHeigth * 0.7
    const circleRadius = screenParams.clusterWidth * 0.7
    for (let i = 0; i < notUsedThreeClusterComponents.length; i++) {
        const clusterComponent = notUsedThreeClusterComponents[i];
        const clusterIds = Object.keys(clusterComponent);
        const angle = Math.random()

        const coords: number[][] = [
            [
                (circleRadius) * (Math.cos(angle * Math.PI)) + middleX + currentX,
                (circleRadius) * (Math.sin(angle * Math.PI)) + middleY + currentY
            ],
            [
                circleRadius * (Math.cos((angle + 2 / 3) * Math.PI)) + middleX + currentX,
                circleRadius * (Math.sin((angle + 2 / 3) * Math.PI)) + middleY + currentY
            ],
            [
                circleRadius * (Math.cos((angle + 4 / 3) * Math.PI)) + middleX + currentX,
                circleRadius * (Math.sin((angle + 4 / 3) * Math.PI)) + middleY + currentY
            ]
        ]

        for (let j = 0; j < 3; j++) {
            finalPositions[clusterIds[j]] = { x: coords[j][0], y: coords[j][1] }
            newRects.push({ left: coords[j][0], top: coords[j][1], width: screenParams.clusterWidth, height: screenParams.clusterHeigth })
        }

        const squareChange = circleRadius * 1.7 + screenParams.clusterWidth
        currentX += squareChange
        if (currentX + squareChange > screenParams.width) {
            currentX = 0
            currentY += squareChange
        }
    }

    /* TWO CLUSTERS COMPONENTS */
    for (let i = 0; i < notUsedTwoClusterComponents.length; i++) {
        const clusterComponent = notUsedTwoClusterComponents[i];
        const clusterIds = Object.keys(clusterComponent);
        const coords: number[][] = [
            [currentX, currentY],
            [currentX + circleRadius * 2, currentY],
        ]

        for (let j = 0; j < 2; j++) {
            finalPositions[clusterIds[j]] = { x: coords[j][0], y: coords[j][1] }
            newRects.push({ left: coords[j][0], top: coords[j][1], width: screenParams.clusterWidth, height: screenParams.clusterHeigth })
        }

        const squareChange = circleRadius * 2 + screenParams.clusterWidth
        currentX += squareChange
        if (currentX > screenParams.width - screenParams.clusterWidth) {
            currentX = 0
            currentY += screenParams.clusterHeigth + 20
        }
    }

    /* ONE CLUSTER COMPONENTS */
    currentY = currentHeight
    for (let i = 0; i < notUsedOneClusterComponents.length; i++) {
        const clusterComponent = notUsedOneClusterComponents[i];
        const clusterId = Object.keys(clusterComponent)[0];
        const clusterRect: Rectangle = { top: currentY, left: currentX, width: screenParams.clusterWidth, height: screenParams.clusterHeigth }

        while (hasCollisions(clusterRect, newRects)) {
            clusterRect.left += screenParams.clusterWidth

            if (clusterRect.left + screenParams.clusterWidth > screenParams.width) {
                clusterRect.left = 0
                clusterRect.top += screenParams.clusterHeigth + 20
            }
        }
        finalPositions[clusterId] = { x: clusterRect.left, y: clusterRect.top }
        currentX = clusterRect.left + screenParams.clusterWidth + 20
        currentY = clusterRect.top
        if (currentX + screenParams.clusterWidth > screenParams.width) {
            currentX = 0
            currentY += screenParams.clusterHeigth
        }
    }
}

/**
 * Calculates maximal height from the positions.
 * @param positions Positions to be taken into account
 * @param clusterHeigth height of the cluster in pixels
 */
export const computeHeigthFromPositions = (
    positions: StringDict<Position>, clusterHeigth: number
): number => {
    let maxY = 0
    for (let key in positions) {
        let pos = positions[key]
        maxY = pos.y > maxY ? pos.y : maxY
    }
    return maxY + clusterHeigth
}


/* 
------------------------------------
The following methods calculate free positions in the graph space 
after the big clusters were placed 
------------------------------------
*/

const _colliding = (start1: number, end1: number, start2: number, end2: number) => {
    return start1 < end2 && start2 < end1;
}

const areColliding = (rect1: Rectangle, rect2: Rectangle) => {
    return _colliding(rect1.left, rect1.left + rect1.width, rect2.left, rect2.left + rect2.width)
        && _colliding(rect1.top, rect1.top + rect1.height, rect2.top, rect2.top + rect2.height);
}

const hasCollisions = (rect: Rectangle, rects: Rectangle[]) => {
    return rects.reduce((res, r) => res || areColliding(r, rect), false);
}

const getPossibleFreePositions = (
    width: number, height: number, clusterWidth: number, clusterHeight: number, rects: Rectangle[]
): { possibleRectanglePositions: Rectangle[], freeRectMatrix: number[][] } => {
    const freeRectMatrix: number[][] = []
    let numberToWidth = Math.floor(width / clusterWidth)
    let numberToheight = Math.floor(height / clusterHeight)

    numberToWidth = numberToWidth === 0 ? 1 : numberToWidth
    numberToheight = numberToheight === 0 ? 1 : numberToheight

    const possibleRectanglePositions = []
    let noRectangles = 0
    for (let i = 0; i < numberToheight; i++) {
        freeRectMatrix[i] = []
        for (let j = 0; j < numberToWidth; j++) {
            const rect: Rectangle = { top: i * clusterHeight, left: j * clusterWidth, width: clusterWidth, height: clusterHeight }
            if (hasCollisions(rect, rects)) {
                freeRectMatrix[i][j] = 0
            }
            else {
                noRectangles += 1
                freeRectMatrix[i][j] = noRectangles
                possibleRectanglePositions.push(rect)
            }
        }
    }

    return { possibleRectanglePositions: possibleRectanglePositions, freeRectMatrix: freeRectMatrix }
}
