import { ArtistFull, Cluster, ArtistEdge, ClusterEdge, ClusterSquare, StringDict, Position, NumberDict } from '../store/types'

/* ALL THE FOLLOWING METHODS ARE PARSING THE JSONS
   THAT WERE RECEIVED FROM THE BACKEND
*/ 
 
interface clusterJson {
    id: string
    genres: string[]
    country: string
    artistsPositions: NumberDict<Position>
}

/** Parses clusters */
export const parseClusters = (artistsDict: StringDict<ArtistFull>, clusterJsons: clusterJson[]): Cluster[] => {
    let clusters: Cluster[] = []
    for (let i = 0; i < clusterJsons.length; i++) {
        const key = clusterJsons[i].id

        // prepare the cluster artists:
        const clusterArtists: ArtistFull[] = []
        const clusterArtistsPositions: StringDict<Position> = {}

        for (const [artistsSpotifyId, artistPosition] of Object.entries(clusterJsons[i].artistsPositions)) {
            clusterArtists.push(artistsDict[artistsSpotifyId])
            clusterArtistsPositions[artistsSpotifyId] = { x: artistPosition.x, y: artistPosition.y }
        }

        clusterArtists.sort((a, b) => { return b.score - a.score })

        const currCluster: Cluster = ({
            id: key,
            genres: clusterJsons[i].genres,
            artistsIds: clusterArtists.map(aFull => aFull.id),
            artistsPositions: clusterArtistsPositions,
            country: clusterJsons[i].country
        })
        clusters.push(currCluster)
    }
    return clusters
}

interface ClusterSquareJSON {
    [id: string]: { x: number, y: number }
}

/** Parses cluster positions */
export const processClusterPositions = (positions: ClusterSquareJSON[]): ClusterSquare[] => {
    let clusterSquares: ClusterSquare[] = []
    for (let i = 0; i < positions.length; i++) {

        const square = positions[i]
        const clusterSquare: ClusterSquare = {};
        if (Object.values(square).length > 0) {
            for (let id in square) {
                let x = square[id].x
                let y = square[id].y
                clusterSquare[id] = { x: x, y: y }
            }
            clusterSquares.push(clusterSquare)
        }
    }

    return clusterSquares
}

interface ClusterEdgeJSON {
    id1: string,
    id2: string,
    weight: number
}

/** Parses cluster edges */
export const parseClusterEdges = (clusterEdgesJsons: ClusterEdgeJSON[]): ClusterEdge[] => {
    return clusterEdgesJsons.map(element => ({ id1: element.id1, id2: element.id2, weight: element.weight })
    )
}

interface ArtistEdgeJSON {
    id1: string,
    id2: string,
    weight: number
}

/** Parses artist edges */
export const parseArtistEdges = (artistEdgesJSONs: ArtistEdgeJSON[]): ArtistEdge[] => {
    let artistEdges: ArtistEdge[] = artistEdgesJSONs.map((edge) => ({ id1: edge.id1, id2: edge.id2, weight: edge.weight }))
    return artistEdges
}
