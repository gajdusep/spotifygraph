import SpotifyWebApi from 'spotify-web-api-js'
import { ArtistFull, ArtistReduced, StringDict, Graph, SongFull } from '../store/types';
import { processClusterPositions, parseClusters, parseClusterEdges, parseArtistEdges } from './graph-parsing-methods'
import { isUndefined } from 'util';

// requests retry interval
const RETRY_INTERVAL = 4000

// parameters for score computing
const isFollowedConst = 5
const likedSongConst = 1
const spotifyPopularityConst = 0.1

const asyncTimeout = (ms: number) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const getCookie = (name: string): string => {
    if (!document.cookie) {
        return ''
    }

    const xsrfCookies = document.cookie.split(';')
        .map(c => c.trim())
        .filter(c => c.startsWith(name + '='));

    if (xsrfCookies.length === 0) {
        return ''
    }
    return decodeURIComponent(xsrfCookies[0].split('=')[1]);
}

/** Translates SpotifyAPI TrackObjectFull to SongFUll */
export const songFromJSONData = (element: SpotifyApi.TrackObjectFull): SongFull => {
    if (element.preview_url === null) {
        console.debug(element.id, ':', element.name, element)
    }

    return {
        name: element.name,
        id: element.id,
        previewLink: element.preview_url,
        artistsIds: element.artists.map(a => a.id),
        externalUrl: element.external_urls.spotify
    }
}

/**
 * Fetches all artists followed by the logged-in user
 * @param sp SpotifyWebApiJs object
 * @returns IDs of followed artists
 */
const getFollowedArtistsIds = async (sp: SpotifyWebApi.SpotifyWebApiJs
): Promise<string[]> => {
    let limit = 50
    let result: string[] = []

    let response
    const maxRetries = 3
    let after = null
    do {
        let success = true
        let retries = 0
        do {
            try {
                const options: {} = after ? { limit: limit, after: after } : { limit: limit }
                response = await sp.getFollowedArtists(options)
            } catch (err) {
                await asyncTimeout(err.getResponseHeader['retry-after'] ? parseInt(err.headers['retry-after'] + 1) * 1000 : RETRY_INTERVAL);
                success = false
                retries += 1
            }
        } while (success === false && retries < maxRetries)

        // the request was not successfull even after retries
        if (isUndefined(response)) {
            after = null
        }
        else {
            result.push(...response.artists.items.map((artObj: SpotifyApi.ArtistObjectFull) => artObj.id))
            after = response.artists.cursors.after
        }
    } while (after)

    return result
}

/**
  * Fetches related artists of the given artists
  * @param sp SpotifyWebApiJs object
  * @param artistsToExploreIds list of artist IDs - we will get related artists information from them
  * @param alreadyLoadedRelatedArtists already saved related artists from the localStorage
  * @param progressBarCallback callback to be called to let progress bar know about the progress
  * @returns list of lists of IDs, the order of related artists is the same as in the artistsToExploreIds list
  */
const getAllRelatedArtists = async (
    sp: SpotifyWebApi.SpotifyWebApiJs,
    artistsToExploreIds: string[],
    alreadyLoadedRelatedArtists: StringDict<string[]>,
    progressBarCallback: Function
): Promise<string[][]> => {
    const result: string[][] = []
    const maxRetries = 3
    const artistsToExploreLen = artistsToExploreIds.length

    for (let i = 0; i < artistsToExploreLen; i++) {
        const aID = artistsToExploreIds[i]
        progressBarCallback(i / artistsToExploreLen)

        if (aID in alreadyLoadedRelatedArtists) {
            result.push(alreadyLoadedRelatedArtists[aID])
        } else {
            let response
            let success = true
            let retries = 0
            do {
                try {
                    response = await sp.getArtistRelatedArtists(aID)
                } catch (err) {
                    await asyncTimeout(err.getResponseHeader['retry-after'] ? parseInt(err.headers['retry-after'] + 1) * 1000 : RETRY_INTERVAL);
                    success = false
                    retries += 1
                }
            } while (success === false && retries < maxRetries)

            if (isUndefined(response)) {
                result.push([])
            }
            else {
                const obtainedRelArtists = response.artists.map((objectFull) => objectFull.id)
                alreadyLoadedRelatedArtists[aID] = obtainedRelArtists
                result.push(obtainedRelArtists)
            }
        }
    }
    return result
}

/**
 * Gets all liked songs of the logged-in user
 * @param sp SpotifyWebApiJs object
 * @returns Promise of list of TrackObjectFull
 */
const getLikedSongs = async (sp: SpotifyWebApi.SpotifyWebApiJs
): Promise<SpotifyApi.TrackObjectFull[]> => {
    let offset = 0
    let limit = 50
    let result: SpotifyApi.TrackObjectFull[] = []

    let response
    const maxRetries = 3
    let next = null
    do {
        let success = true
        let retries = 0
        do {
            try {
                response = await sp.getMySavedTracks({ limit: 50, offset: offset })
            } catch (err) {
                await asyncTimeout(err.getResponseHeader['retry-after'] ? parseInt(err.headers['retry-after'] + 1) * 1000 : RETRY_INTERVAL);
                success = false
                retries += 1
            }
        } while (success === false && retries < maxRetries)

        // the request was not successfull even after retries
        if (isUndefined(response)) {
            next = null
        }
        else {
            offset += limit
            result.push(...response.items.map((trackObj: SpotifyApi.SavedTrackObject) => trackObj.track))
            next = response.next
        }
    } while (next)

    return result
}

/**
 * Separates artist IDs from the track objects
 * @param likedSongs list of TrackObjectFull
 * @returns list of IDs of artists in the likedSongs
 */
const getArtistsIdsFromLikedSongs = (likedSongs: SpotifyApi.TrackObjectFull[]) => {
    const likedSongsArtistsIdsNonDistinct =
        likedSongs.map((objectTrackFull) => objectTrackFull.artists)
            .reduce((acc, currVal) => acc.concat(currVal), [])
            .map((artistObjectSimplified) => artistObjectSimplified.id)
    return [...new Set(likedSongsArtistsIdsNonDistinct)]
}

/**
 * Fetches the full information about the artists given in parameter
 * @param sp SpotifyWebApiJs object
 * @param allArtistsIds list of IDs of artists to be obtained
 * @returns list of ArtistObjectFull
 */
export const getAllArtistsFull = async (sp: SpotifyWebApi.SpotifyWebApiJs, allArtistsIds: string[])
    : Promise<SpotifyApi.ArtistObjectFull[]> => {
    const result: SpotifyApi.ArtistObjectFull[] = []
    const maxRetries = 3
    let offset = 0
    let limit = 50

    do {
        let response
        let success = true
        let retries = 0
        do {
            try {
                response = await sp.getArtists(allArtistsIds.slice(offset, offset + limit))
            } catch (err) {
                await asyncTimeout(err.getResponseHeader['retry-after'] ? parseInt(err.headers['retry-after'] + 1) * 1000 : RETRY_INTERVAL);
                success = false
                retries += 1
            }
        } while (success === false && retries < maxRetries)

        if (isUndefined(response)) {
            throw new Error("Unable to load artists.")
        } else {
            result.push(...response.artists)
            offset += limit
        }

    } while (offset < allArtistsIds.length)

    return result
}

/**
 * Creates Artist Dictionary: [artistId]:ArtistFull
 * @param artistsFull ArtistObjectFull objects
 * @param relatedArtistsFinal related artists corresponding to the artistsFull list
 * @param followedArtistsIdsSet IDs of artists that are followed by the logged-in user
 * @param likedSongs songs liked by the logged-in user
 */
const createArtistDictWithFullInformation = (
    artistsFull: SpotifyApi.ArtistObjectFull[],
    relatedArtistsFinal: string[][],
    followedArtistsIdsSet: Set<string>,
    likedSongs: SpotifyApi.TrackObjectFull[]
): StringDict<ArtistFull> => {
    if (artistsFull.length !== relatedArtistsFinal.length) {
        console.debug('ERROR: NOT EVERY ARTIST HAS RELATED ARTISTS')
        return {}
    }

    // add every artists to the dictionary
    const dictToReturn: StringDict<ArtistFull> = {}
    for (let i = 0; i < artistsFull.length; i++) {
        const artist = artistsFull[i];
        const artistImage = artist.images && artist.images.length > 0 ? artist.images[artist.images.length - 1].url : ''
        const isFollowed = followedArtistsIdsSet.has(artist.id)

        const popularity = artist.popularity
        const followedScore = isFollowed ? isFollowedConst : 0

        dictToReturn[artist.id] = {
            genres: artist.genres,
            id: artist.id,
            image: artistImage,
            isFollowed: isFollowed,
            likedSongs: [],
            name: artist.name,
            relatedArtistsIds: relatedArtistsFinal[i],
            score: popularity * spotifyPopularityConst + followedScore
        }
    }

    // add liked songs information to the artists
    for (let i = 0; i < likedSongs.length; i++) {
        const song = likedSongs[i];
        for (let j = 0; j < song.artists.length; j++) {
            const songArtist = song.artists[j]
            if (songArtist.id in dictToReturn) {
                dictToReturn[songArtist.id].likedSongs.push({
                    artistsIds: song.artists.map(simplArtist => simplArtist.id),
                    externalUrl: song.external_urls.spotify,
                    name: song.name,
                    id: song.id,
                    previewLink: song.preview_url
                })
            }
        }
    }

    // recomputed artists' score
    for (let key in dictToReturn) {
        const art = dictToReturn[key]
        dictToReturn[key].score += art.likedSongs.length * likedSongConst
    }

    return dictToReturn
}

const dictArtistFullToReduced =
    (artistsDictFull: StringDict<ArtistFull>): StringDict<ArtistReduced> => {
        const artistsDictReduced: StringDict<ArtistReduced> = {}
        for (let key in artistsDictFull) {
            const artFull = artistsDictFull[key]
            artistsDictReduced[key] = {
                id: artFull.id,
                relatedArtistsIds: artFull.relatedArtistsIds,
                genres: artFull.genres
            }
        }
        return artistsDictReduced
    }

const emptyGraph = (): Graph => {
    return { clusters: [], clusterEdges: [], artistEdges: [], clusterSquares: [], artistDict: {} }
}

/**
 * Fetches all the necessary graph data.
 * @param accessToken Spotify access token
 * @param sp SpotifyWebApiJs object
 * @param setGraphLoadingMessageFunction callback informing about the loading
 * @param setGraphLoadingProgress callback informing about the progress
 * @param setGraphLoadingError callback informing about errors
 */
export const fetchAllDataFromSpotify = async (
    accessToken: string,
    sp: SpotifyWebApi.SpotifyWebApiJs,
    setGraphLoadingMessageFunction: Function,
    setGraphLoadingProgress: Function,
    setGraphLoadingError: Function
): Promise<Graph> => {
    /*
    1) REQ - liked songs
    2) artists from liked songs
    3) REQ - followed artists
    4) concat from_liked and followed_artists
    5) REQ - get all artists full objects
    6) REQ - get related artists of concatenated
    7) make a dict, {artistsId: artist} - where artist is full artist object (or our Artist type)
    8) add additional info to the artists:
        a)OK for every liked song - add to dict the songs
        b)OK is artist followed?
        c) calculate artist score
        d)OK related artists ids (for future work)
    */

    if (!!!accessToken) return emptyGraph()

    sp.setAccessToken(accessToken)

    // 1) get liked songs
    const likedSongs = await getLikedSongs(sp)

    setGraphLoadingMessageFunction("Getting your liked songs from Spotify...")
    console.debug('liked songs: ', likedSongs)

    // 2) artists from liked songs
    const likedSongsArtistsIds = getArtistsIdsFromLikedSongs(likedSongs)
    console.debug('liked songs artists ids: ', likedSongsArtistsIds)

    // 3) followed artists
    setGraphLoadingMessageFunction("Getting the artists you follow from Spotify...")
    const followedArtistsIds = await getFollowedArtistsIds(sp)
    console.debug('followed artists ids: ', followedArtistsIds)

    // 4) concat from_liked and followed_artists
    const allArtistsIds = [...new Set(likedSongsArtistsIds.concat(followedArtistsIds))]
    // const allArtistsIds = [...new Set(followedArtistsIds)] // debug option for less ids...
    console.debug('all artists ids: ', allArtistsIds)

    if (allArtistsIds.length < 1) {
        setGraphLoadingError("Your graph is empty, we have nothing to show to you")
        return emptyGraph()
    }

    // 5) get all artists full objects
    setGraphLoadingMessageFunction("Getting information about all the artists you are interested it...")
    const artistsFull = await Promise.resolve(getAllArtistsFull(sp, allArtistsIds))
    console.debug('artists full: ', artistsFull);

    // 6) get related artists of concatenated
    const relatedArtistsStorageName = "relatedArtists"
    // localStorage.setItem(relatedArtistsStorageName, JSON.stringify({})) // ONLY FOR DEBUGGING
    const storageRelatedArtistsValue = localStorage.getItem(relatedArtistsStorageName)

    let alreadyLoadedRelatedArtists = {}
    if (storageRelatedArtistsValue === null) {

    }
    else {
        try {
            alreadyLoadedRelatedArtists = JSON.parse(storageRelatedArtistsValue)
        } catch {
            alreadyLoadedRelatedArtists = {}
        }

        if (alreadyLoadedRelatedArtists === null) {
            alreadyLoadedRelatedArtists = {}
        }
    }


    setGraphLoadingMessageFunction("Getting information about the related artists of all of your favourite artists... \n This might take a while :(")
    // const relatedArtistsFinal = await Promise.resolve(getRelatedArtists(sp, allArtistsIds, 0, alreadyLoadedRelatedArtists, setGraphLoadingProgress))
    const relatedArtistsFinal = await getAllRelatedArtists(sp, allArtistsIds, alreadyLoadedRelatedArtists, setGraphLoadingProgress)
    localStorage.setItem(relatedArtistsStorageName, JSON.stringify(alreadyLoadedRelatedArtists))
    console.debug('related artists final', relatedArtistsFinal)

    // 7,8) make a dict, add aditional info
    const artistDict = createArtistDictWithFullInformation(
        artistsFull,
        relatedArtistsFinal,
        new Set(followedArtistsIds),
        likedSongs
    )
    console.debug('artist dict', artistDict)

    const csfrtoken = getCookie('csrftoken')

    setGraphLoadingMessageFunction("We have all the data we need, now computing the graph!")

    let calculateGraphLink = process.env.NODE_ENV === "production" ? process.env.REACT_APP_PROD_BACKEND_URL_CALCULATE_GRAPH : process.env.REACT_APP_DEV_BACKEND_URL_CALCULATE_GRAPH
    if (isUndefined(calculateGraphLink)) return emptyGraph()

    let jsonReponse: any = {}
    try {
        const fetchReponse = await fetch(calculateGraphLink, {
            method: 'POST',
            body: JSON.stringify(dictArtistFullToReduced(artistDict)),
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csfrtoken,
                // 'X-Api-Key': accessToken
            }
        })
        if (fetchReponse.ok) {
            jsonReponse = await fetchReponse.json()
        } else {
            throw new Error(fetchReponse.status + '.  ' + fetchReponse.statusText + '.  ' + await fetchReponse.text())
        }
    } catch (requestError) {
        setGraphLoadingError(requestError)
        throw requestError
    }
    
    console.debug(jsonReponse)

    const artistEdges = parseArtistEdges(jsonReponse.artistEdges)

    const clusters = parseClusters(artistDict, jsonReponse.clusters)

    const clusterEdges = parseClusterEdges(jsonReponse.clusterEdges)

    const clusterSquares = processClusterPositions(jsonReponse.clusterSquares)

    const g = new Promise<Graph>(resolve => resolve({
        artistEdges: artistEdges,
        clusters: clusters,
        clusterEdges: clusterEdges,
        clusterSquares: clusterSquares,
        artistDict: artistDict,
    }
    ))

    console.debug('graph from spotify fetch:', await g)
    return g
}
