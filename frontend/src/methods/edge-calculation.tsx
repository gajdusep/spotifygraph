
/**
 * Receives positions of nodes. Recomputes the coordinates to be working well with 
 * svg element in cluster and artist edges.
 * 
 * @param x1 X coordinate of first node
 * @param y1 Y coordinate of first node
 * @param x2 X coordinate of second node
 * @param y2 Y coordinate of second node
 * @param thickness thickness of the edge
 */
export const computeEdgeCoordinates = (x1: number, y1: number, x2: number, y2: number, thickness: number): 
{ x1fin: number, y1fin: number, x2fin: number, y2fin: number, top: number, left: number, hei: number, wid: number } => {

    let top: number, left: number, wid: number, hei: number

    if (x1 > x2) {
        left = x2
        wid = x1 - x2
    }
    else {
        left = x1
        wid = x2 - x1
    }

    if (y1 > y2) {
        top = y2
        hei = y1 - y2
    }
    else {
        top = y1
        hei = y2 - y1
    }

    let x1fin = 0 + thickness
    let y1fin = hei
    let x2fin = wid
    let y2fin = 0 + thickness
    if ((y1 > y2 && x1 > x2) ||
        (y1 < y2 && x1 < x2)) {
        y1fin = 0 + thickness
        y2fin = hei
    }

    return { x1fin, y1fin, x2fin, y2fin, top, left, hei, wid }
}
