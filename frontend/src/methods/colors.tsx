
/** The most commonly used colors in SpotifyGraph */
const COLORS = {
    lightAzureBlue: `rgb(132, 237, 243)`,
    darkAzureBlue: `rgb(2, 109, 104)`,
    pink: `rgb(255, 51, 153)`,
    green: `rgb(0, 204, 0)`,
    grey: `rgb(191, 191, 191)`,
    orange: `rgb(242,225,31)`
}

export default COLORS;
