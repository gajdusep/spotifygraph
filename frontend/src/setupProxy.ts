
const proxy = require('http-proxy-middleware')
    
export default function(app: any) {
  app.use(
    proxy('/**', {
      target: 'https://accounts.spotify.com',
      changeOrigin: true
    })
  );
}
