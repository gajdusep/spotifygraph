import React from 'react'
import { computeEdgeCoordinates } from '../methods/edge-calculation'

interface ArtistEdgeProps {
    /** X coordinate of the first artist */
    x1: number
    /** Y coordinate of the first artist */
    y1: number
    /** X coordinate of the second artist */
    x2: number
    /** Y coordinate of the second artist*/
    y2: number
    /** Weight of the edge */
    weigth: number
    /** Artist element width */
    artistWidth: number
    /** Weight of the strongest edge in the Inside cluster graph */
    maximumEdgeWeigth: number
}

/** ArtistEdge - edge between artists in the Inside cluster graph */
export default class ArtistEdge extends React.Component<ArtistEdgeProps, {}> {
    render() {
        const thickness = this.props.weigth * 2
        const { x1fin, y1fin, x2fin, y2fin, top, left, hei, wid } = computeEdgeCoordinates(
            this.props.x1, this.props.y1, this.props.x2, this.props.y2, thickness)

        const st: React.CSSProperties = {
            top: top + 30,
            left: left + this.props.artistWidth / 2
        }

        const curvature = 20
        const edgeColor = `rgb(${2}, ${250}, ${240}, ${this.props.weigth / this.props.maximumEdgeWeigth})`

        return (
            <svg style={st} className="position-absolute" width={wid + thickness + curvature} height={hei + thickness + curvature}>
                <path
                    d={`M ${x1fin},${y1fin} 
                        C ${x1fin + curvature},${y1fin + curvature} ${x2fin + curvature},${y2fin + curvature} ${x2fin},${y2fin}`}
                    strokeWidth={thickness}
                    fill="none"
                    stroke={edgeColor}
                />
            </svg>
        )
    }
}
