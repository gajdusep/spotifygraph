import React from 'react'
import { connect } from 'react-redux'
import Draggable, { DraggableEvent } from 'react-draggable'
import ReactCountryFlag from 'react-country-flag'
import CSS from 'csstype';

import { State } from '../store/types'
import { setChosenCluster, } from '../store/actions'
import { Cluster, StringDict, ArtistFull } from '../store/types'
import './cluster-node.css'

var countries = require("i18n-iso-countries");
countries.registerLocale(require("i18n-iso-countries/langs/en.json"));

interface ClusterNodeProps {
    /** Cluster to be shown as node */
    cluster: Cluster
    /** Initial X coordinate */
    positionX: number
    /** Initial Y coordinate */
    positionY: number
    /** Callback - let Main graph know that position of the node with artist_id was changed */
    positionChanged: (x: number, y: number, artist_id: string) => void
    /** Set chosen cluster in the State */
    setChosenCluster: typeof setChosenCluster
    
    /** Constant to adapt description font size on zoom */
    fontSizeConst: number
    /** Width of the cluster in the default zoom. */
    clusterWidth: number
    /** Artist dictionarty - [artistId]:artistFullObject */
    artistDict: StringDict<ArtistFull>
}

interface ClusterNodeState {
    /** Current X coordinate */
    positionX: number
    /** Current Y coordinate */
    positionY: number
    /** Is the node being dragged */
    dragging: boolean
    /** The default font size of the description */
    fontSizeDefault: number
}

class ClusterNode extends React.Component<ClusterNodeProps, ClusterNodeState> {
    constructor(props: ClusterNodeProps) {
        super(props);
        this.state = {
            positionX: props.positionX,
            positionY: props.positionY,
            dragging: false,
            fontSizeDefault: 12
        }
        this.onDrag = this.onDrag.bind(this)
        this.onStop = this.onStop.bind(this)
    }

    componentDidUpdate(prevProps: ClusterNodeProps, prevState: ClusterNodeState) {
        if (prevProps.positionX !== this.props.positionX) {
            this.setState({ positionX: this.props.positionX })
        }
    }

    /** On node drag -> change position and call callback to let Main graph edges know */
    onDrag(e: DraggableEvent, ui: any) {
        e.stopPropagation(); e.preventDefault();

        const oldX = this.state.positionX
        const oldY = this.state.positionY

        const newX = oldX + ui.deltaX
        const newY = oldY + ui.deltaY

        this.setState({ dragging: true, positionX: newX, positionY: newY })
        this.props.positionChanged(newX, newY, this.props.cluster.id)
    }

    onStop(e: DraggableEvent) {
        e.stopPropagation(); e.preventDefault();
        const dragging = this.state.dragging;
        this.setState({ dragging: false })
        if (!dragging) {
            this.handleClick(e);
        }
    }

    handleClick(e: any) {
        this.props.setChosenCluster(this.props.cluster)
    }

    createArtistWrapper(title: string, style: {}, key: number): JSX.Element {
        return (
            <div key={key} className="artistWrapper">
                <div className="colourWrapper">
                    <div title={title} style={style} className="artistDiv"></div>
                </div>
            </div>
        )
    }

    createEmptyWrapper(key: number): JSX.Element {
        return <div key={key} className="artistWrapper"><div></div></div>
    }
    
    /** Prepares the node div.
     * Makes rows of artists
     */
    clusterDiv(): JSX.Element {
        const imgSize = this.props.clusterWidth / 4.6
        let artistDivStyle: CSS.Properties = {
            width: `${imgSize}px`,
            height: `${imgSize}px`,
            backgroundSize: `${imgSize}px ${imgSize}px`,
        }

        const rows: JSX.Element[] = [];
        const numberOfArtists = this.props.cluster.artistsIds.length

        let currKey = 0
        let l: JSX.Element[] = []
        for (let i = 0; i < Math.min(numberOfArtists, 4); i++) {
            const currArtist = this.props.artistDict[this.props.cluster.artistsIds[i]]
            const artistDivStyleNew = {
                ...artistDivStyle,
                backgroundImage: `url(${currArtist.image})`
            }
            l.push(this.createArtistWrapper(currArtist.name, artistDivStyleNew, currKey++))
        }
        rows.push(<div key={1} className="artistsRow">{l}</div>)

        l = []
        if (numberOfArtists > 4) {
            l.push(this.createEmptyWrapper(currKey++))
            l.push(this.createEmptyWrapper(currKey++))

            for (let i = 4; i < Math.min(6, numberOfArtists); i++) {
                const currArtist = this.props.artistDict[this.props.cluster.artistsIds[i]]
                const artistDivStyleNew = {
                    ...artistDivStyle,
                    backgroundImage: `url(${currArtist.image})`
                }
                l.push(this.createArtistWrapper(currArtist.name, artistDivStyleNew, currKey++))
            }

            if (numberOfArtists > 6) {
                const artistDivStyleNew = {
                    ...artistDivStyle,
                    backgroundImage: `url(${process.env.PUBLIC_URL + '/three-dots.png'})`
                }
                l.push(this.createArtistWrapper("AND OTHERS", artistDivStyleNew, currKey++))
            }
            l.push(this.createEmptyWrapper(currKey++))
            l.push(this.createEmptyWrapper(currKey++))
            rows.push(<div key={2} className="artistsRow">{l}</div>)
        }

        let headerString = ""
        const numberOfGenres = this.props.cluster.genres.length
        if (numberOfGenres === 0) {
            headerString = "...unknown genre..."
        }
        else {
            for (let i = 0; i < Math.min(numberOfGenres, this.props.fontSizeConst + 1); i++) {
                if (i === 0) {
                    headerString += this.props.cluster.genres[i]
                }
                else {
                    headerString += ', ' + this.props.cluster.genres[i]
                }
            }
        }

        let wrapperStyle: CSS.Properties = {
            width: `${this.props.clusterWidth}px`,
        }
        return (<div className="nodeWrapper" style={wrapperStyle}>
            {
                this.props.cluster.country &&
                <div style={{
                    position: "absolute",
                    margin: 0,
                    left: 0,
                    opacity: 0.4,
                    zIndex: -1,
                    alignItems: 'center',
                }}>
                    <ReactCountryFlag
                        countryCode={countries.alpha3ToAlpha2(this.props.cluster.country)}
                        svg
                        style={{
                            width: this.props.clusterWidth + 30,
                            height: '260px'
                        }}
                    />
                </div>
            }

            <div className="nodeHeader">
                <div className="header noselect" style={{ fontSize: this.state.fontSizeDefault / this.props.fontSizeConst + this.props.fontSizeConst }}>
                    {headerString}
                </div>
            </div>
            {rows}

        </div>)
    }

    render() {
        const clusterDiv = this.clusterDiv()
        return (
            <Draggable
                onDrag={this.onDrag}
                onStop={this.onStop}
                onStart={(e: DraggableEvent) => { e.stopPropagation(); e.preventDefault(); }}
                bounds="parent"
                defaultPosition={{ x: this.props.positionX, y: this.props.positionY }}
                position={{ x: this.state.positionX, y: this.state.positionY }}
            >
                {clusterDiv}
            </Draggable>
        )
    }
}

export default connect(
    (state: State) => ({
        artistDict: state.graph.artistDict
    }),
    {
        setChosenCluster,
    }
)(ClusterNode)
