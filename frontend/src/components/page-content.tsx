import * as React from 'react'

import DraggableCanvas from './draggable-canvas'
import Player from './player'

import './page-content.css'

/** Wrapper to DraggableCanvas and Player */
class PageContent extends React.Component<{}, {}> {
    render() {
        return (
            <div className="mainDiv">
                <div className="mainDivContent">
                    <DraggableCanvas/>
                </div>
                <Player/>
            </div>
        )
    }
}

export default PageContent
