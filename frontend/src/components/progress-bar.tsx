/* eslint-disable @typescript-eslint/no-useless-constructor */

import React from 'react'

interface ProgressBarProps {
    progress: number
}

export default class ProgressBar extends React.Component<ProgressBarProps, {}> {
    constructor(props: ProgressBarProps) {
        super(props);
    }

    render() {
        return (
            <div className="progressbar">
                <div className="progress" style={{ width: `${this.props.progress * 100}%` }} />
            </div>
        )
    }
}
