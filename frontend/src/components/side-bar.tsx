import * as React from 'react'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import { State, StringDict, ArtistFull, Cluster, Graph } from '../store/types'
import { loadGraphFromFile } from '../store/actions'

import './side-bar.css'
import './styles.css'

interface SideBarProps {
    /** Chosen cluster if any, otherwise undefined */
    chosenCluster?: Cluster
    /** Graph object */
    graph: Graph
    /** ArtistDict: [artistId]: ArtistFull */
    artistDict: StringDict<ArtistFull>
    /** Function to load parse the graph from the file */
    loadGraphFromFile: Function
    /** Was graph loaded */
    graphLoaded: boolean

    graphLoadingError: boolean
}

interface SideBarState { }

class SideBar extends React.Component<SideBarProps, SideBarState> {
    constructor(props: SideBarProps) {
        super(props);
        this.dowloadGraph = this.dowloadGraph.bind(this)
        this.uploadGraph = this.uploadGraph.bind(this)
    }

    /** Downloads graph to json file */
    dowloadGraph() {
        if (!this.props.graph) return

        const element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.props.graph, null, 4)));
        element.setAttribute('download', 'mySpotifyGraph.json');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

    /** Loads a graph from a given file */
    uploadGraph(selectorFiles: FileList) {
        const corruptedFileAlert = 'Sorry, this file seems to be not in the correct format.\n Error:'
        console.log(selectorFiles)
        const file = selectorFiles[0]
        const reader = new FileReader()
        const loadGraphFromFileFunc = (g: Graph) => (this.props.loadGraphFromFile(g))

        reader.onload = function (event) {
            if (event.target !== null && event.target.result !== null) {
                try {
                    const resultString = event.target.result.toString()
                    const resultJSON = isUndefined(resultString) ? {} : JSON.parse(resultString)
                    const g: Graph = resultJSON
                    loadGraphFromFileFunc(g)
                } catch (error) {
                    alert(corruptedFileAlert + error)
                    console.debug('error while graph loading from file:', error)
                }
            }
            else {
                alert(corruptedFileAlert)
            }
        };

        reader.readAsText(file);
    }

    /** Enlists names of artists of the chosen cluster */
    createChosenClusterArtists(): JSX.Element {
        if (isUndefined(this.props.chosenCluster)) return <div></div>

        return (
            <ul>{this.props.chosenCluster.artistsIds.map(id =>
                <li className="shift-left">
                    {this.props.artistDict[id].name}
                </li>)}
            </ul>
        )
    }

    /** Enlists genres of artists of the chosen cluster */
    createChosenClusterGenres(): JSX.Element {
        let properties: JSX.Element[] = []
        if (this.props.chosenCluster !== undefined) {
            properties = this.props.chosenCluster.genres.map(a => <li className="shift-left">{a}</li>)
        }
        return <ul>{properties}</ul>
    }

    render() {
        return (
            <div className="sideBar">
                <div className="sideBarHeader">
                    <img width={40} height={40} className="spotify-image" alt="" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGQ9Ik0xOS4wOTggMTAuNjM4Yy0zLjg2OC0yLjI5Ny0xMC4yNDgtMi41MDgtMTMuOTQxLTEuMzg3LS41OTMuMTgtMS4yMi0uMTU1LTEuMzk5LS43NDgtLjE4LS41OTMuMTU0LTEuMjIuNzQ4LTEuNCA0LjIzOS0xLjI4NyAxMS4yODUtMS4wMzggMTUuNzM4IDEuNjA1LjUzMy4zMTcuNzA4IDEuMDA1LjM5MiAxLjUzOC0uMzE2LjUzMy0xLjAwNS43MDktMS41MzguMzkyem0tLjEyNiAzLjQwM2MtLjI3Mi40NC0uODQ3LjU3OC0xLjI4Ny4zMDgtMy4yMjUtMS45ODItOC4xNDItMi41NTctMTEuOTU4LTEuMzk5LS40OTQuMTUtMS4wMTctLjEyOS0xLjE2Ny0uNjIzLS4xNDktLjQ5NS4xMy0xLjAxNi42MjQtMS4xNjcgNC4zNTgtMS4zMjIgOS43NzYtLjY4MiAxMy40OCAxLjU5NS40NC4yNy41NzguODQ3LjMwOCAxLjI4NnptLTEuNDY5IDMuMjY3Yy0uMjE1LjM1NC0uNjc2LjQ2NS0xLjAyOC4yNDktMi44MTgtMS43MjItNi4zNjUtMi4xMTEtMTAuNTQyLTEuMTU3LS40MDIuMDkyLS44MDMtLjE2LS44OTUtLjU2Mi0uMDkyLS40MDMuMTU5LS44MDQuNTYyLS44OTYgNC41NzEtMS4wNDUgOC40OTItLjU5NSAxMS42NTUgMS4zMzguMzUzLjIxNS40NjQuNjc2LjI0OCAxLjAyOHptLTUuNTAzLTE3LjMwOGMtNi42MjcgMC0xMiA1LjM3My0xMiAxMiAwIDYuNjI4IDUuMzczIDEyIDEyIDEyIDYuNjI4IDAgMTItNS4zNzIgMTItMTIgMC02LjYyNy01LjM3Mi0xMi0xMi0xMnoiLz48L3N2Zz4="></img>
                    <h1>Spotify graph</h1>
                </div>
                <div>Graph of your musical taste ♬♫</div>

                {
                    this.props.graphLoaded &&
                    <div className="download-button pointer" onClick={this.dowloadGraph}>
                        Download graph to JSON
                    </div>
                }
                {
                    (this.props.graphLoaded || this.props.graphLoadingError )&&
                    <input className="download-button" type="file" accept=".json" onChange={(e) => {
                        if (e.target.files !== null) this.uploadGraph(e.target.files)
                    }} />
                }

                <div className="sideBarContent">
                    {
                        this.props.chosenCluster &&
                        <div className="box-with-margin">
                            <h2>mostly these genres</h2>
                            {this.createChosenClusterGenres()}
                        </div>
                    }
                    {
                        this.props.chosenCluster &&
                        <div className="box-with-margin">
                            <h2 className="text-align-center">group of artists</h2>
                            {this.createChosenClusterArtists()}
                        </div>
                    }

                    {!!!this.props.chosenCluster && this.props.graph &&
                        <div className="box-with-margin">
                            <p>We took the artists from your <b>liked</b> songs and the artists that <b>you follow</b>.</p>
                            <p>We found <b>{Object.values(this.props.graph.artistDict).length} artist</b> that you like.</p>
                            <p>We divided them into clusters you're seing right now. The clusters are based on <b>genres</b> and <b>preferences</b> of other users of Spotify</p>
                            <p>Click the cluster and explore how the artists are connected.</p>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default connect(
    (state: State) => ({
        chosenCluster: state.chosenCluster,
        graph: state.graph,
        sp: state.spotifyWebApiObject,
        artistDict: state.graph.artistDict,
        graphLoaded: state.graphLoaded,
        graphLoadingError: state.isGraphLoadingError
    }),
    {
        loadGraphFromFile
    }
)(SideBar)
