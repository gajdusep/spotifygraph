import React, { ChangeEvent } from 'react'
import { connect } from 'react-redux'
import Draggable from 'react-draggable'
import { TiArrowMove } from 'react-icons/ti'
import { BsQuestion } from 'react-icons/bs'

import { State, ArtistFull, StringDict } from '../store/types'
import { InsideClusterGraph } from './chosen-cluster'
import ArtistEdge from './artist-edge'
import ArtistNode from './artist-node'

import './chosen-cluster-graph.css'

interface ChosenClusterGraphProps {
    /** Inside cluster graph object containing nodes and edges */
    graphElements: InsideClusterGraph

    /** Dictionary: [artistId]: artistFullObject */
    idArtistDict: StringDict<ArtistFull>
}

interface SliderLimits {
    minimum: number
    maximum: number
}

interface ChosenClusterGraphState {
    /** Width of the div with inside cluster graph */
    width: number
    /** Height of the div with inside cluster graph */
    height: number
    /** JSX node elements */
    nodes: JSX.Element[]
    /** Position of artists in the graph */
    artistsPositions: { [id: string]: { x: number, y: number } }

    /** current limits of the artist slider */
    artistsSliderLimits: SliderLimits
    /** current limits of the edges slider */
    edgesSliderLimits: SliderLimits
    /** current value of the artist slider */
    artistSliderValue: number
    /** current value of the edges slider */
    edgesSliderValue: number

    /** is the description shown right now? */
    descriptionHover: boolean

    /** minimal edge weight for the edge to be shown */
    minimumEdgeWeight: number
    /** maximal number of edges to be visualized by default */
    maximumEdges: number
}

class ChosenClusterGraph extends React.Component<ChosenClusterGraphProps, ChosenClusterGraphState> {
    boxRef: any
    wrapperRef: any

    constructor(props: ChosenClusterGraphProps) {
        super(props);
        const maximumEdges = 400
        const { artistsSliderLimits, edgesSliderLimits, currSliderValue } = this.getSliderLimits(maximumEdges)
        this.state = {
            width: 0, height: 0,
            nodes: [], artistsPositions: {},
            artistSliderValue: artistsSliderLimits.minimum,
            edgesSliderValue: currSliderValue,
            artistsSliderLimits: artistsSliderLimits,
            edgesSliderLimits: edgesSliderLimits,
            descriptionHover: false,
            minimumEdgeWeight: 0.5,
            maximumEdges: maximumEdges
        }
        this.wrapperRef = React.createRef();
        this.boxRef = React.createRef();

        this.positionChangedHandler = this.positionChangedHandler.bind(this)
        this.changeArtistSliderValue = this.changeArtistSliderValue.bind(this)
        this.changeEdgeSliderValue = this.changeEdgeSliderValue.bind(this)
    }    

    /**
     * If a new Inside cluster graph must appear -> create a new graph
     * 
     * If a slider value is changed -> nodes are created again with new threshold
     */
    componentDidUpdate(prevprops: ChosenClusterGraphProps, prevstate: ChosenClusterGraphState) {
        if (prevprops.graphElements !== this.props.graphElements) {
            this.prepareAfterGraphDataUpdate()
        }
        else if (prevstate.artistSliderValue !== this.state.artistSliderValue) {
            this.prepareAfterSliderUpdate()
        }
    }

    /**
     * After a component did mount -> create a new graph
     */
    componentDidMount() {
        this.prepareAfterGraphDataUpdate()
    }

    /**
     * Create a new graph.
     */
    prepareAfterGraphDataUpdate() {
        const { artistsSliderLimits, edgesSliderLimits, currSliderValue } = this.getSliderLimits(this.state.maximumEdges)
        this.setState({
            artistsSliderLimits: artistsSliderLimits,
            edgesSliderLimits: edgesSliderLimits,
            artistSliderValue: artistsSliderLimits.minimum,
            edgesSliderValue: currSliderValue
        })
        let width = this.boxRef.current.clientWidth;
        let height = this.boxRef.current.clientHeight;
        this.setState({ width: width, height: height })
        const createdNodes: { nodes: JSX.Element[], positions: {} } = this.createNodes(width, height);
        this.setState({ nodes: createdNodes.nodes, artistsPositions: createdNodes.positions })
    }

    /**
     * Prepare nodes with current positions and new slider value. Sets state - new nodes
     */
    prepareAfterSliderUpdate() {
        const nodes = []
        for (let i = 0; i < this.props.graphElements.nodes.length; i++) {
            const elem = this.props.graphElements.nodes[i]
            nodes.push(
                <ArtistNode
                    key={elem.artist.id}
                    positionChangedCallback={this.positionChangedHandler}
                    positionX={this.state.artistsPositions[elem.artist.id].x}
                    positionY={this.state.artistsPositions[elem.artist.id].y}
                    artist={elem.artist}
                    minimumScore={this.state.artistSliderValue}
                />
            )
        }
        this.setState({ nodes: nodes })
    }

    /**
     * Newly created Inside cluster graph -> new nodes.
     * 
     * Creates nodes from the initial positions in props.graphElements
     * 
     * @param wid Inside cluster graph width
     * @param hei Inside cluster graph height
     */
    createNodes(wid: number, hei: number): { nodes: JSX.Element[], positions: {} } {
        const nodeWidth = 100
        const nodeHeight = 100
        const canvasPadding = 15

        const nodes: JSX.Element[] = []
        let newArtistsPositions: { [id: string]: { x: number, y: number } } = {}
        for (let i = 0; i < this.props.graphElements.nodes.length; i++) {
            const elem = this.props.graphElements.nodes[i]
            const compX = elem.position.x * (wid - nodeWidth - 2 * canvasPadding) + canvasPadding
            const compY = elem.position.y * (hei - nodeHeight - 2 * canvasPadding) + canvasPadding
            newArtistsPositions[elem.artist.id] = { x: compX, y: compY }

            nodes.push(
                <ArtistNode
                    key={elem.artist.id}
                    positionChangedCallback={this.positionChangedHandler}
                    positionX={compX}
                    positionY={compY}
                    artist={elem.artist}
                    minimumScore={this.state.artistSliderValue}
                />
            )
        }
        return { nodes, positions: newArtistsPositions }
    }

    /**
     * Calculates artist and edge slider extrema
     * 
     * @param maximumEdges maximal number of defaultly visible edges
     */
    getSliderLimits(maximumEdges: number): {
        artistsSliderLimits: SliderLimits,
        edgesSliderLimits: SliderLimits,
        currSliderValue: number
    } {
        let artistsSliderLimits: SliderLimits = { minimum: 1000, maximum: -1000 }
        let edgesSliderLimits: SliderLimits = { minimum: 1000, maximum: -1000 }

        // get slider values
        for (let i = 0; i < this.props.graphElements.nodes.length; i++) {
            const node = this.props.graphElements.nodes[i]
            const artistScore = node.artist.score
            artistsSliderLimits.maximum = artistScore > artistsSliderLimits.maximum ? artistScore : artistsSliderLimits.maximum
            artistsSliderLimits.minimum = artistScore < artistsSliderLimits.minimum ? artistScore : artistsSliderLimits.minimum
        }

        const edgesValues: number[] = this.props.graphElements.edges.map(el => el.weigth).sort((a, b) => a - b)
        edgesSliderLimits.minimum = edgesValues[0]
        edgesSliderLimits.maximum = edgesValues[edgesValues.length - 1]

        artistsSliderLimits.maximum = Math.ceil(artistsSliderLimits.maximum)
        artistsSliderLimits.minimum = Math.ceil(artistsSliderLimits.minimum) - 1
        edgesSliderLimits.maximum = Math.ceil(edgesSliderLimits.maximum)
        edgesSliderLimits.minimum = Math.ceil(edgesSliderLimits.minimum) - 1

        const l = edgesValues.length
        const currSliderValue = l > maximumEdges ? edgesValues[l - maximumEdges] + 0.1 : edgesSliderLimits.minimum

        return {
            artistsSliderLimits: artistsSliderLimits,
            edgesSliderLimits: edgesSliderLimits,
            currSliderValue: currSliderValue
        }
    }

    /**
     * Changes state with the new position value of node.
     * @param x new X coordinate
     * @param y new Y coordinate
     * @param artist_id ID of the node that will be changed
     */
    positionChangedHandler(x: number, y: number, artist_id: string) {
        if (this.state.artistsPositions == null) {
            return []
        }
        const pos = this.state.artistsPositions
        pos[artist_id] = { x: x, y: y }
        this.setState({ artistsPositions: pos });
    }

    /**
     * Returns edges created from node positions.
     */
    createEdges(): JSX.Element[] {
        const edges = []
        if (this.state.artistsPositions == null) {
            return []
        }

        try {
            for (let i = 0; i < this.props.graphElements.edges.length; i++) {
                const edge = this.props.graphElements.edges[i]
                const node1id = edge.id1
                const node2id = edge.id2
                const edgeWeigth = edge.weigth
                const clusterEdgeKey = node1id + node2id

                const node1 = this.props.idArtistDict[node1id]
                const node2 = this.props.idArtistDict[node2id]

                if (node1 !== node2 && edgeWeigth > this.state.edgesSliderValue &&
                    edgeWeigth > this.state.minimumEdgeWeight &&
                    node1.score >= this.state.artistSliderValue &&
                    node2.score >= this.state.artistSliderValue
                ) {
                    edges.push(
                        <ArtistEdge
                            key={clusterEdgeKey}
                            x1={this.state.artistsPositions[node1id].x}
                            y1={this.state.artistsPositions[node1id].y}
                            x2={this.state.artistsPositions[node2id].x}
                            y2={this.state.artistsPositions[node2id].y}
                            weigth={edge.weigth}
                            artistWidth={70}
                            maximumEdgeWeigth={this.state.edgesSliderLimits.maximum}
                        />
                    )
                }
            }
        }
        catch (err) {
            edges.push(<div></div>)
        }

        return edges
    }

    changeArtistSliderValue(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ artistSliderValue: e.currentTarget.valueAsNumber })
    }

    createArtistSlider(): JSX.Element {
        const minimum = this.state.artistsSliderLimits.minimum
        const maximum = this.state.artistsSliderLimits.maximum
        return <div className="inner-slider-wrapper">
            <input
                type="range"
                min={minimum}
                max={maximum}
                step={(maximum - minimum) / 10}
                value={this.state.artistSliderValue}
                onChange={this.changeArtistSliderValue}
                className="slider-input"
            />
        </div>
    }

    changeEdgeSliderValue(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ edgesSliderValue: e.currentTarget.valueAsNumber })
    }

    createEdgeSlider(): JSX.Element {
        const minimum = Math.max(this.state.edgesSliderLimits.minimum, this.state.minimumEdgeWeight)
        const maximum = this.state.edgesSliderLimits.maximum
        return <div className="inner-slider-wrapper">
            <input
                type="range"
                min={minimum}
                max={maximum}
                step={(maximum - minimum) / 10}
                value={this.state.edgesSliderValue}
                onChange={this.changeEdgeSliderValue}
                className="slider-input"
            />
        </div>
    }

    changeHover() {
        this.setState({ descriptionHover: !this.state.descriptionHover })
    }

    getDraggableSlider() {
        const showDescriptionStyle = this.state.descriptionHover ? {} : { display: 'none' }
        return (
            <Draggable handle="strong" bounds="parent">
                <div className="draggable-slider-div">
                    <div>
                        <div className="outer-slider-wrapper">
                            {this.createEdgeSlider()}
                            edges
                        </div>
                        <div className="outer-slider-wrapper">
                            {this.createArtistSlider()}
                            artists
                        </div>
                    </div>
                    <BsQuestion onMouseEnter={() => this.changeHover()} onMouseLeave={() => this.changeHover()} size={30} />
                    <strong><TiArrowMove size={30} /></strong>
                    <div className="slider-description" style={showDescriptionStyle}>
                        <p>Drag the sliders to control how many edges and artists will be shown.</p>
                        <p>The artists will be hidden in the order based on their importance to you - less important first, more important remainig.</p>
                        <p>The importance takes into account these factors: how many their songs you like, if you follow them and popularity on Spotify.</p>
                        <p>The score of an edge between two artists is calculated from the number of common genres and Spotify "related artists" information.</p>
                    </div>
                </div>
            </Draggable>
        )
    }

    render() {
        return (
            <div className="noselect chosen-cluster-wrapper" ref={this.wrapperRef}>
                <div className="insideClusterBox" ref={this.boxRef} style={{ width: this.state.width }}>
                    {this.getDraggableSlider()}
                    {this.state.width !== 0 && this.state.nodes.length !== 0 && this.createEdges()}
                    {this.state.width !== 0 && this.state.nodes}
                </div>
            </div>
        )
    }
}

export default connect(
    (state: State) => ({
        idArtistDict: state.graph.artistDict
    }),
    {}
)(ChosenClusterGraph)
