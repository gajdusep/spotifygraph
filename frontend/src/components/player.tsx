import React from 'react'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import { State, SongFull, StringDict, ArtistSimplified, ArtistFull } from '../store/types'
import { setPaused } from '../store/actions'

import './draggable-canvas.css'
import './styles.css'

interface PlayerProps {
    /** Access token to check */
    accessToken?: string
    /** Song that will be played */
    songToPlay?: SongFull
    /** Is paused? */
    paused: boolean
    /** Function to pause the player */
    setPaused: Function
    /** Dict of artists that can appear in the not-liked songs. Necessary to show artists' names. */
    nonFavouriteArtistDict: StringDict<ArtistSimplified>
    /** Dict of artists in the user's graph */
    favouriteArtistDict: StringDict<ArtistFull>
}

/** Audio player that plays the song given in the state.songToPlay */
class Player extends React.Component<PlayerProps, {}> {
    playerRef: any
    constructor(props: any) {
        super(props);
        this.playerRef = React.createRef();
    }

    componentDidUpdate() {
        if (this.playerRef.current !== null) {
            if (this.props.paused) {
                this.playerRef.current.pause()
            }
            else {
                this.playerRef.current.play()
            }
        }
    }
        
    render() {
        if (isUndefined(this.props.accessToken)) {
            return (
                <div></div>
            )
        }
        else if (isUndefined(this.props.songToPlay)) {
            return (
                <div className="empty-player">
                    <div className="generic-description-small-margin">Choose some artist and some song and listen to a preview</div>
                </div>
            )
        }
        else {
            let mp3Link = ''
            let artistsDescriptionNames = []
            for (let i = 0; i < this.props.songToPlay.artistsIds.length; i++) {
                const aID = this.props.songToPlay.artistsIds[i];
                if (aID in this.props.favouriteArtistDict) {
                    artistsDescriptionNames.push(this.props.favouriteArtistDict[aID].name)
                }
                else if (aID in this.props.nonFavouriteArtistDict) {
                    artistsDescriptionNames.push(this.props.nonFavouriteArtistDict[aID].name)
                }
                else {
                    artistsDescriptionNames.push(aID)
                }
            }
            const artistsDescription = artistsDescriptionNames.join(', ')
            mp3Link = this.props.songToPlay.previewLink == null ? '' : this.props.songToPlay.previewLink

            return (
                <div className="player">
                    <div className="flex1">
                        <p className="playingSongName"><b>{this.props.songToPlay.name}</b></p>
                        <p className="playingSongArtists">{artistsDescription}</p>
                    </div>
                    <audio onPlay={() => this.props.setPaused(false)}
                        onPause={() => this.props.setPaused(true)}
                        ref={this.playerRef}
                        autoPlay controls src={mp3Link} className="playerControls"
                    >
                        Your browser does not support the
                        <code>audio</code> element.
                    </audio>
                </div>
            )
        }
    }
}

export default connect(
    (state: State) => ({
        paused: state.paused,
        accessToken: state.accessToken,
        songToPlay: state.songToPlay,
        favouriteArtistDict: state.graph.artistDict,
        nonFavouriteArtistDict: state.nonFavouriteArtistDict
    }),
    {
        setPaused
    }
)(Player)
