import React from 'react'
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux'
import { withRouter } from "react-router-dom";
import { RouteComponentProps } from "react-router";

import PageContent from './page-content'
import SideBar from './side-bar'
import { State } from '../store/types'
import { setAccessToken } from '../store/actions'

import '../App.css'

interface MyGraphPageProps extends RouteComponentProps<any> {
    setAccessToken: typeof setAccessToken
}

interface MyGraphPageState {
    loggingLoading: boolean
    loggingFailed: boolean
}

/** The base page. Wrapper to SideBar and PageContent */
class MyGraphPage extends React.Component<MyGraphPageProps, MyGraphPageState> {
    constructor(props: MyGraphPageProps) {
        super(props);
        this.state = {
            loggingLoading: true,
            loggingFailed: false
        }
        /* eslint-disable react/no-direct-mutation-state */
        this.afterSpotifyLogin()
    }

    afterSpotifyLogin() {
        const urlParams = window.location.search
        var query = urlParams.substr(1);
        var result: any = {};
        query.split("&").forEach(function (part: string) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        })
        if (!('code' in result) || result['code'] === '' || result['code'] === null) {
            this.state = { loggingFailed: true, loggingLoading: false }
            return
        }

        const currentPathName = this.props.history.location.pathname
        this.props.history.push({
            pathname: currentPathName,
            search: ''
        })

        const linkToBackendWithoutCode = process.env.NODE_ENV === "production" ? process.env.REACT_APP_PROD_BACKEND_URL_LOGGED_IN : process.env.REACT_APP_DEV_BACKEND_URL_LOGGED_IN
        const linkToBackend = linkToBackendWithoutCode + result['code']
        let accessToken: string = ''
        fetch(linkToBackend)
            .then((response) => {
                if (response.status !== 200) {
                    throw response.status
                }
                const tryAccessToken = response.headers.get('access-token')
                if (tryAccessToken !== null) {
                    accessToken = tryAccessToken
                    this.props.setAccessToken(accessToken)
                }

                this.setState({ loggingLoading: false })
            })
            .catch((error) => {
                console.debug(error)
                this.setState({ loggingFailed: true, loggingLoading: false })
                alert('Sorry, we did not manage to log you in Spotify.')
            })
    }

    render() {
        if (this.state.loggingLoading) {
            return (
                <div className="App">
                    <div className="page-wrapper">
                        <div className="loader" />
                        <div className="generic-description">Logging into Spotify</div>
                    </div>
                </div>
            )
        }

        if (this.state.loggingFailed) {
            return <Redirect to="/login" />
        }

        return (
            <div className="App">
                <SideBar />
                <PageContent />
            </div>
        )
    }
}

export default withRouter<MyGraphPageProps, any>(
    connect(
        (state: State) => ({}),
        (dispatch: any) => ({
            setAccessToken: (accessToken: string) => dispatch(setAccessToken(accessToken)),
        })
    )(MyGraphPage)
)
