import React from 'react'
import {computeEdgeCoordinates} from '../methods/edge-calculation'

interface ClusterEdgeProps {
    /** X coordinate of the first cluster */
    x1: number
    /** Y coordinate of the first cluster */
    y1: number
    /** X coordinate of the second cluster */
    x2: number
    /** Y coordinate of the second cluster */
    y2: number
    /** Weight of the edge */
    weigth: number
    /** Size of the cluster to calculate the starting coordinates of edge */
    clusterWidth: number
}

/** ClusterEdge - edge between clusters in the Main graph */
export default class ClusterEdge extends React.Component<ClusterEdgeProps, {}> {

    render() {
        const thickness = this.props.weigth
        const {x1fin, y1fin, x2fin, y2fin, top, left, hei, wid} = computeEdgeCoordinates(
            this.props.x1, this.props.y1, this.props.x2, this.props.y2, thickness)

        const st: React.CSSProperties ={
            top: top + 30,
            left: left + this.props.clusterWidth / 2
        }

        const curvature = 20

        return(
            <svg style={st} className="position-absolute" width={wid+thickness+curvature} height={hei+thickness+curvature}>
                <path
                    d={`M ${x1fin},${y1fin}
                        C ${x1fin+curvature},${y1fin+curvature} ${x2fin+curvature},${y2fin+curvature} ${x2fin},${y2fin}`}
                    strokeWidth={thickness}
                    fill="none"
                    stroke="black"
                />
            </svg>
        )
    }
}
