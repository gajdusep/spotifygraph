/* eslint-disable @typescript-eslint/no-useless-constructor */

import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { RouteComponentProps } from 'react-router'

import { State } from '../store/types'

import './welcome-page.css'
import '../App.css'

interface WelcomePageProps extends RouteComponentProps<any> {
    loggedIn: boolean
}

class WelcomePage extends React.Component<WelcomePageProps, {}> {
    constructor(props: WelcomePageProps) {
        super(props);
    }

    render() {
        return (
            <div className="App">
                <div className="page-wrapper">
                    <div className="page-description-div">
                        <h1>Spotify graph</h1>
                        <p>A project that will show you your musical taste.</p>
                        <p>We will fetch data from your spotify account, divide them into groups and visualize these groups in form of a graph.</p>
                    </div>
                    <button className="very-big-button" onClick={() => { this.props.history.push('/login') }}>Continue to get your graph</button>
                    <div className="generic-description">
                        <p>Have a look at the code on: <a href="https://gitlab.com/gajdusep/spotifygraph" target="_blank">GitLab</a></p>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter<WelcomePageProps, any>(
    connect(
        (state: State) => ({
            loggedIn: state.userLoggedIn
        }),
        {}
    )(WelcomePage))
