
import React from 'react'
import { connect } from 'react-redux'
import { isUndefined } from 'util'
import { MdPlayCircleOutline, MdPauseCircleOutline } from 'react-icons/md'
import { IoMdMusicalNotes, IoMdHeart } from 'react-icons/io'
import { RiQuestionLine} from 'react-icons/ri'
import SpotifyWebApi from 'spotify-web-api-js'

import { ArtistFull, State, ChosenArtistData, SongFull } from '../store/types'
import { setSongToPlay, setPaused } from '../store/actions'
import COLORS from '../methods/colors'

import './chosen-artist-bar.css'
import './styles.css'

interface ChosenArtistBarProps {
    /** Chosen artist - full object */
    chosenArtist?: ArtistFull,
    /** Chosen artist loading - true if loading */
    chosenArtistLoading: boolean,
    /** Chosen artist error - if some error occurs, contains error message */
    chosenArtistError?: string,
    /** Chosen artist data object */
    chosenArtistData?: ChosenArtistData,
    /** Function to play a song */
    setSongToPlay: (song: SongFull) => void,    
    /** Spotify Web API object */
    spotifyApiObject: SpotifyWebApi.SpotifyWebApiJs,
    /** Song that is currently in the player */
    songToPlay: SongFull
    /** Is song paused? */
    paused: boolean
    /** Change the state of player*/
    setPause: (paused: boolean) => void
}

interface ChosenArtistBarState {
    chosenSongId: string
    likedSongs:SongFull[]
    notLikedSongs:SongFull[]
}

/**
 * Right bar with Chosen artist information (songs, image)
 */
class ChosenArtistBar extends React.Component<ChosenArtistBarProps, ChosenArtistBarState> {

    constructor(props: any) {
        super(props)
        const {liked, notLiked} = this.prepareSongs()
        this.state = {chosenSongId: '', likedSongs:liked, notLikedSongs: notLiked}
    }

    setChosenSong(songId: string) {
        if (songId === this.state.chosenSongId) {
            this.setState({chosenSongId: ""})
        }
        else {
            this.setState({chosenSongId: songId})
        }
    }

    componentDidUpdate(prevProps: ChosenArtistBarProps, prevState: ChosenArtistBarState) {
        if(prevProps.chosenArtistData !== this.props.chosenArtistData && this.props.chosenArtistData) {
            this.setState({
                chosenSongId: '',
                likedSongs:this.props.chosenArtistData.likedSongs,
                notLikedSongs: this.props.chosenArtistData.notLikedSongs
            })
        }
    }

    prepareSongs(): {liked: SongFull[], notLiked: SongFull[]} {
        if(!!!this.props.chosenArtistData || isUndefined(this.props.chosenArtist)) {
            return {liked:[],notLiked:[]}
        }

        const likedSongs:SongFull[] = []
        const notLikedSongs:SongFull[] = []

        for (let i = 0; i < this.props.chosenArtistData.likedSongs.length; i++) {
            const element = this.props.chosenArtistData.likedSongs[i];
            if (this.props.chosenArtist.likedSongs && !!!likedSongs.includes(element)) {
                likedSongs.push(element);
            }
            else if (!notLikedSongs.includes(element)){
                notLikedSongs.push(element);
            }
        }
        console.debug('liked:', likedSongs)
        console.debug('liked by artist', this.props.chosenArtist.likedSongs)
        console.debug('not liked:', notLikedSongs)
        return {liked: likedSongs, notLiked: notLikedSongs}
    }

    getSongDiv(song: SongFull, liked: boolean): JSX.Element {
        const style = this.state.chosenSongId === song.id ? {maxHeight: 80} : {maxHeight: 0}

        let playerIcon = <MdPlayCircleOutline size={40} onClick={(e) => {
            e.stopPropagation(); 
            this.props.setSongToPlay(song);
        }} />

        let chosenSongColorIndicator = COLORS.lightAzureBlue;
        if (this.props.songToPlay && this.props.songToPlay.id === song.id) {
            chosenSongColorIndicator = COLORS.pink;
            if (this.props.paused) {
                playerIcon = <MdPlayCircleOutline size={40} onClick={(e) => {e.stopPropagation(); this.props.setPause(false)}} />
            }
            else {
                playerIcon = <MdPauseCircleOutline size={40} onClick={(e) => {e.stopPropagation(); this.props.setPause(true)}} />
            }
        }

        let likedSongColorIndicator = COLORS.lightAzureBlue
        if (liked) {
            likedSongColorIndicator = COLORS.green
        }

        if (song.previewLink === '' || song.previewLink === null || isUndefined(song.previewLink)) {
            playerIcon = <RiQuestionLine size={40} onClick={(e) => {e.stopPropagation(); alert('No preview found on Spotify')}} />
            chosenSongColorIndicator = COLORS.grey
        }

        return (
            <div key={song.id} className="flex-column song-div" style={{backgroundColor: chosenSongColorIndicator}}>
                <div className="song-preview" style={{backgroundColor: likedSongColorIndicator}} onClick={() => this.setChosenSong(song.id)}>
                    <div className="pointer"
                        style={{marginRight:"5px", display:"inline-flex", flexDirection:"column", justifyContent:"center", alignContent:"center"}}
                    >
                        {playerIcon}
                    </div>
                    <div className="song-wrapper">
                        {song.name}
                    </div>
                </div>
                <div className="song-description" style={style}>
                    <div className="full-song-link-div" onClick={() => {window.open(song.externalUrl)}}>
                        <IoMdMusicalNotes/>
                        Full song
                    </div>
                    { !!!liked && <div className="full-song-link-div" onClick={() => {
                        this.props.spotifyApiObject.addToMySavedTracks([song.id])
                        .then(() => {
                            console.debug('TRACK SAVED')
                            let likedSongsOld = this.state.likedSongs
                            likedSongsOld = likedSongsOld.concat(song)
                            let notLikedSongsOld = this.state.notLikedSongs.filter(s => s.id !== song.id)

                            this.setState({likedSongs: likedSongsOld, notLikedSongs: notLikedSongsOld})
                        })
                        .catch((error: any) => {
                            console.debug('THIS ERROR DURING TRACK SAVING OCCURED:', error)
                        })
                    }}>
                        <IoMdHeart/>
                        Save to liked songs
                    </div>
                    }
                </div>
            </div>
        )
    }

    getSongsDivs(songs: SongFull[], liked: boolean): JSX.Element[] {
        if (!!!songs) return []

        const toReturn: JSX.Element[] = []
        for (let i = 0; i < songs.length; i++) {
            toReturn.push(
                this.getSongDiv(songs[i], liked)
            )
        }
        return toReturn

    }

    render() {
        if (this.props.chosenArtistError) {
            return (
                <div className="chosen-artist-bar">
                    <h2><i>{this.props.chosenArtistError}</i></h2>
                </div>
            )
        }
        else if (isUndefined(this.props.chosenArtist) || isUndefined(this.props.chosenArtistData)) {
            return (
                <div className="chosen-artist-bar">
                    <h2>CLICK SOME ARTIST</h2>
                </div>
            )
        }
        else if (this.props.chosenArtistLoading) {
            return (
                <div className="chosen-artist-bar">
                    <h2>LOADING...</h2>
                </div>
            )
        }
        else {
            return (
                <div className="chosen-artist-bar">
                    <div className="chosen-artist-bar-top">
                        <h1 style={{margin:0}}>
                            {this.props.chosenArtist.name}
                        </h1>
                        <div style={{
                            backgroundImage:`url(${this.props.chosenArtist.image})`,
                            width: 70, height: 70, borderRadius: `${20}%`,
                            backgroundSize: `70px 70px`,
                            display: "inline-grid"
                        }}></div>
                        {
                            this.props.chosenArtist.isFollowed &&
                            <h2 style={{ margin:`5px 0px 0px 0px` }}>You are following this artist.</h2>
                        }
                    </div>
                    <div className="chosen-artist-bar-scroll">
                        {this.getSongsDivs(this.state.likedSongs, true)}
                        {this.getSongsDivs(this.state.notLikedSongs, false)}
                    </div>
                </div>
            )
        }
    }
}

export default connect(
    (state: State) => ({
        songToPlay: state.songToPlay,
        chosenArtist: state.chosenArtist,
        chosenArtistLoading: state.chosenArtistLoading,
        chosenArtistError: state.chosenArtistError,
        chosenArtistData: state.chosenArtistData,
        spotifyApiObject: state.spotifyWebApiObject,
        paused: state.paused
    }),
    (dispatch: Function) => ({
        setPause: (paused:boolean) => dispatch(setPaused(paused)),
        setSongToPlay: (song: SongFull) => dispatch(setSongToPlay(song))
    })
)(ChosenArtistBar)
