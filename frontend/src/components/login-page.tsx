/* eslint-disable @typescript-eslint/no-useless-constructor */

import React from 'react'

import './welcome-page.css'
import '../App.css'

interface LoginPageProps { }

interface LoginPageState {}

/** Login page that redirects to Spotify login page. */
export default class LoginPage extends React.Component<LoginPageProps, LoginPageState> {
    constructor(props: LoginPageProps) {
        super(props);
    }

    /** Creates Spotify login link with all necessary parameters */
    spotifyLoginLink() {
        const client_id = 'dfb05fa5c36b41d48407ad5e3773d39b'
        const scope = [
            'streaming',
            'user-read-email',
            'user-read-private',
            'user-read-playback-state',
            'user-modify-playback-state',
            'user-library-read',
            'user-library-modify',
            'user-follow-read',
            'playlist-modify-private',
            'playlist-read-private',
        ]
        const state = 'rndstringsomehash'
        const redirect_uri = process.env.NODE_ENV === "production" ?
            process.env.REACT_APP_PROD_SPOTIFY_REDIRECT_URL : process.env.REACT_APP_DEV_SPOTIFY_REDIRECT_URL
        const params = {
            client_id:client_id,
            response_type:'code',
            redirect_uri:redirect_uri,
            scope:scope.join(' '),
            state:state,
        }

        const encodeGetParams = (p:Object): string =>
            Object.entries(p).map(kv => kv.map(encodeURIComponent).join("=")).join("&");

        const link = 'https://accounts.spotify.com/authorize?' + encodeGetParams(params)
        return link
    }

    loginWithSpotify() {
        window.location.assign(this.spotifyLoginLink())
    }

    render() {
        return (
            <div className="App">
                <div className="page-wrapper">
                    <div className="generic-description">
                        <div>Please login with your spotify account</div>
                    </div>
                    <button className="very-big-button" onClick={() => this.loginWithSpotify()}>Log in with your Spotify account</button>
                </div>
            </div>
        )
    }
}
