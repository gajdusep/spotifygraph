import * as React from 'react'
import CSS from 'csstype'
import './styles.css'
import { connect } from 'react-redux'
import {TiArrowBack} from 'react-icons/ti'
import { State, Cluster, ClusterEdge, ArtistEdge,ArtistFull, StringDict } from '../store/types'
import { closeChosenClusterGraph, setChosenCluster, closeChosenArtist } from '../store/actions'
import ChosenClusterGraph from './chosen-cluster-graph'
import ChosenArtistBar from './chosen-artist-bar'
import RecommendationPanel from './recommendation-panel'

interface ChosenClusterProps {
    closeChosenClusterGraph: Function,
    closeChosenArtist: Function,
    setChosenCluster: Function,
    chosenCluster: Cluster,
    clusters: Cluster[],
    clusterEdges: ClusterEdge[],
    artistEdges: ArtistEdge[],
    artistDict: StringDict<ArtistFull>
}

interface ChosenClusterState {
    elements: InsideClusterGraph
}

export interface InsideClusterArtistNode {
    artist: ArtistFull
    position: {x:number, y:number}
}

export interface InsideClusterArtistEdge {
    id: string
    id1: string
    id2: string
    weigth: number
}

export interface InsideClusterGraph {
    nodes: InsideClusterArtistNode[]
    edges: InsideClusterArtistEdge[]
}

class ChosenCluster extends React.Component<ChosenClusterProps, ChosenClusterState> {
    constructor(props: any) {
        super(props);
        this.state = {
            elements:this.getChosenClusterGraphElements(this.props.chosenCluster)
        }
    }

    getChosenClusterGraphElements(chosenCluster: Cluster): InsideClusterGraph {
        const nodes: InsideClusterArtistNode[] = [];
        const edges: InsideClusterArtistEdge[] = [];

        for (let i = 0; i < chosenCluster.artistsIds.length; i++) {
            const currArtistId = chosenCluster.artistsIds[i];
            const pos = chosenCluster.artistsPositions[currArtistId]
            nodes.push({artist:this.props.artistDict[currArtistId], position: {x:pos.x, y:pos.y}})
        }

        for (let i = 0; i < this.props.artistEdges.length; i++) {
            const id1 = this.props.artistEdges[i].id1;
            const id2 = this.props.artistEdges[i].id2;
            if (chosenCluster.artistsIds.includes(id1) && chosenCluster.artistsIds.includes(id2)) {
                edges.push({id: id1+id2, id1: id1, id2: id2, weigth: this.props.artistEdges[i].weight})
            }
        }

        return {nodes, edges}
    }

    getSimilarClusters(): JSX.Element[] {
        const id = this.props.chosenCluster.id
        const clusterEdgesWithChosenCluster: ClusterEdge[] = []

        for (let i = 0; i < this.props.clusterEdges.length; i++) {
            const clusterEdge = this.props.clusterEdges[i]
            if (clusterEdge.id2 !== clusterEdge.id1 &&
                (clusterEdge.id1 === id || clusterEdge.id2 === id)) {
                clusterEdgesWithChosenCluster.push(clusterEdge)
            }
        }
        clusterEdgesWithChosenCluster.sort((a, b) => a.weight > b.weight ? -1 : 1)

        const similarClusters: JSX.Element[] = []
        const minimalClusterSimilarity = 2

        const imgWid = 60
        const imgHei = 60
        let artistDivStyle: CSS.Properties = {
            width: `${imgWid}px`,
            height: `${imgHei}px`,
            backgroundSize: `${imgHei}px ${imgWid}px`,
        }

        for (let i = 0; i < clusterEdgesWithChosenCluster.length; i++) {
            const edge = clusterEdgesWithChosenCluster[i];
            if (edge.weight > minimalClusterSimilarity) {
                const secondId = id === edge.id1 ? edge.id2 : edge.id1
                const similarCluster = this.props.clusters[this.props.clusters.map(e => e.id).indexOf(secondId)]
                const similarClusterArtistsElements: JSX.Element[] = []

                for (let j = 0; j < Math.min(similarCluster.artistsIds.length, 3); j++) {
                    const artistId = similarCluster.artistsIds[j];
                    const currArtist = this.props.artistDict[artistId]
                    const artistDivStyleNew = {
                        ...artistDivStyle,
                        backgroundImage:`url(${currArtist.image})`
                    }
                    similarClusterArtistsElements.push(
                        <div className="artistWrapper">
                            <div title={currArtist.name} style={artistDivStyleNew} className="artistDiv"></div>
                        </div>
                    )
                }

                similarClusters.push(
                    <div className="similar-cluster-div padding-margin pointer" onClick={
                            () => {
                                this.props.closeChosenArtist()
                                this.props.setChosenCluster(similarCluster)
                                this.setState({
                                    elements:this.getChosenClusterGraphElements(similarCluster)
                                })
                            }
                        }>
                        <p>Similar to: {similarCluster.genres[0]}</p>
                        <div className="artistsRow">
                            {similarClusterArtistsElements}
                        </div>
                    </div>
                )
            }
        }
        return similarClusters
    }

    onClick = () => {
        this.props.closeChosenClusterGraph()
        this.props.closeChosenArtist()
    }

    render() {
        const similarClusters = this.getSimilarClusters()

        return (
            <div className="chosen-cluster-div">
                <div className="chosen-cluster-column">
                    <div className="similar-cluster-row">
                        <div className="go-back-div padding-margin pointer" onClick={this.onClick}>
                            <TiArrowBack size={50}/>
                            <h3 className="h3-no-padding">Go back to the graph</h3>
                        </div>
                        {similarClusters}
                    </div>
                    <div className="chosen-cluster-graph-div">
                        <ChosenClusterGraph graphElements={this.state.elements}/>
                    </div>

                    <RecommendationPanel clusterArtistsIds={this.props.chosenCluster.artistsIds}/>
                </div>

                <ChosenArtistBar/>
            </div>
        )
    }
}

const mapStateToProps = (state: State) => {
    return {
        artistEdges: state.graph.artistEdges,
        clusters: state.graph.clusters,
        clusterEdges: state.graph.clusterEdges,
        artistDict: state.graph.artistDict
    }
}

export default connect(
    mapStateToProps,
    {
        closeChosenClusterGraph,
        closeChosenArtist,
        setChosenCluster
    }
  )(ChosenCluster)
