/* eslint-disable @typescript-eslint/no-useless-constructor */

import React from 'react'
import SpotifyWebApi from 'spotify-web-api-js'
import { connect } from 'react-redux'
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { isUndefined } from 'util';

import { Graph, Cluster, State, StringDict, Position } from '../store/types'
import { setGraph, loadGraph, graphFromFileIsLoaded } from '../store/actions'
import { computeHeigthFromPositions, computeClusterPositions } from '../methods/graph-layout'
import ChosenCluster from './chosen-cluster'
import ClusterNode from './cluster-node'
import ClusterEdge from './cluster-edge'
import ProgressBar from './progress-bar'

import './draggable-canvas.css'

interface DraggableCanvasProps {
    /** Thunk action for loading graph from the backend */
    loadGraph: typeof loadGraph
    /** The graph object */
    graph: Graph
    /** Was the graph loaded */
    graphLoaded: boolean
    /** Is the graph loading */
    graphLoading: boolean
    /** Message during the loading */
    graphLoadingMessage: string,
    /** Progress of related artists */
    graphLoadingRelatedArtistsProgress: number,
    /** Did error occur */
    isGraphLoadingError: boolean
    /** Message if error occured */
    graphLoadingErrorMessage: string,
    /** The chosen cluster */
    chosenCluster: Cluster
    /** Spotify API access token */
    accessToken: string
    /** SpotifyWebApiJs object */
    sp: SpotifyWebApi.SpotifyWebApiJs
    /** Set if a graph from file is loaded */
    graphFromFileIsLoaded: Function
    /** Is graph loaded from file */
    isGraphFromFileLoaded: boolean
}

interface DraggableCanvasState {
    /** Dimensions of draggable canvas */
    dimensions?: { w: number, h: number }
    /** Current positions of clusters */
    clusterPositions?: StringDict<Position>
    /** Width of one cluster */
    clusterWidth: number
    /** Approximate height of one cluster (it can differ if cluster contains less artists) */
    clusterHeight: number
    /** Current zoom scale */
    currentScale: number

    /** Draggable canvas width when did mount */
    originalRefWidth: number
    /** Draggable canvas height when did mount */
    originalRefHeight: number
}

/**
 * Contains all the work with the Main graph.
 */
class DraggableCanvas extends React.Component<DraggableCanvasProps, DraggableCanvasState> {
    boxRef: any
    wrapperRef: any

    constructor(props: any) {
        super(props);
        this.state = {
            clusterWidth: 150,
            clusterHeight: 150,
            currentScale: 1,
            originalRefHeight: 0,
            originalRefWidth: 0
        }

        this.wrapperRef = React.createRef();
        this.boxRef = React.createRef();

        this.positionChangedHandler = this.positionChangedHandler.bind(this)
    }

    positionChangedHandler(x: number, y: number, artist_id: string) {
        if (this.state.clusterPositions == null) {
            return []
        }
        const pos = this.state.clusterPositions
        pos[artist_id] = { x: x, y: y }
        this.setState({ clusterPositions: pos });
    }

    graphLoadedCallback(width: number, height: number) {
        const clusterWidth = this.state.clusterWidth
        const clusterHeigth = this.state.clusterHeight

        let newHeight = height
        if (this.props.graph.clusters.length < 15) { newHeight -= (clusterHeigth + 20) }
        else if (this.props.graph.clusters.length > 30) { newHeight += clusterHeigth }
        const positions = computeClusterPositions(width, newHeight, clusterWidth, clusterHeigth, this.props.graph.clusterSquares)
        const newHeigth = computeHeigthFromPositions(positions, clusterHeigth);

        const currDimensions = this.state.dimensions ? this.state.dimensions : { w: 500, h: 1000 }
        currDimensions.h = newHeigth
        this.setState({ clusterPositions: positions })
    }

    /** On width change -> recompute nodes positions */
    onResizeRecomputePositions() {
        if (isUndefined(this.state.dimensions)) return
        if (isUndefined(this.state.clusterPositions)) return

        const origWidth = this.state.dimensions.w
        const origHei = this.state.dimensions.h
        const newWidth = this.wrapperRef.current.clientWidth

        if (origWidth === newWidth) return

        const currentPositions = this.state.clusterPositions
        const ratio = Math.floor((newWidth / origWidth) * 1024) / 1024
        for (let key in currentPositions) {
            currentPositions[key].x = ratio * currentPositions[key].x
        }

        this.setState({
            dimensions: { w: newWidth, h: origHei },
            clusterPositions: currentPositions
        })
    }

    componentDidMount() {
        window.addEventListener('resize', () => { this.onResizeRecomputePositions() });
        let width = this.wrapperRef.current.clientWidth;
        let height = this.wrapperRef.current.clientHeight;
        this.setState(
            {
                dimensions: { w: width, h: height },
                originalRefWidth: width,
                originalRefHeight: height
            },
            () => this.props.loadGraph(this.props.accessToken, this.props.sp, () => this.graphLoadedCallback(width, height))
        )
    }

    componentDidUpdate(prevProps: DraggableCanvasProps, prevState: DraggableCanvasState) {
        if (this.props.isGraphFromFileLoaded) {
            let w = 500
            let h = 500
            if (!isUndefined(this.state.dimensions)) {
                w = this.state.dimensions.w
                h = this.state.dimensions.h
            }
            // w = this.state.originalRefWidth 
            h = this.state.originalRefHeight
            this.graphLoadedCallback(w, h)
            this.props.graphFromFileIsLoaded(false)
        }
    }

    /** Creates node elements */
    createNodes(): JSX.Element[] {
        if (this.state.clusterPositions === null || isUndefined(this.state.clusterPositions)) {
            return []
        }

        const nodes = []
        for (let i = 0; i < this.props.graph.clusters.length; i++) {
            const element = this.props.graph.clusters[i];
            nodes.push(
                <ClusterNode
                    key={element.id}
                    fontSizeConst={this.state.currentScale}
                    cluster={element}
                    positionX={this.state.clusterPositions[element.id].x}
                    positionY={this.state.clusterPositions[element.id].y}
                    positionChanged={this.positionChangedHandler}
                    clusterWidth={this.state.clusterWidth}
                />
            )
        }
        return nodes
    }

    /** Creates edge elements */
    createEdges(): JSX.Element[] {
        const edges = []
        if (this.state.clusterPositions == null) {
            return []
        }

        for (let i = 0; i < this.props.graph.clusterEdges.length; i++) {
            const source = this.props.graph.clusterEdges[i].id1
            const target = this.props.graph.clusterEdges[i].id2
            const clusterEdgeKey = source + target
            if (source !== target) {
                const edgeWeigth = Math.log2(this.props.graph.clusterEdges[i].weight) * 3
                edges.push(
                    <ClusterEdge
                        key={clusterEdgeKey}
                        x1={this.state.clusterPositions[source].x}
                        y1={this.state.clusterPositions[source].y}
                        x2={this.state.clusterPositions[target].x}
                        y2={this.state.clusterPositions[target].y}
                        weigth={edgeWeigth}
                        clusterWidth={this.state.clusterWidth}
                    />
                )
            }
        }

        return edges
    }

    createGraphLoadingDiv() {
        return (
            <div className="page-wrapper">
                <div className="generic-description">{this.props.graphLoadingMessage}</div>
                <ProgressBar progress={this.props.graphLoadingRelatedArtistsProgress} />
            </div>
        )
    }

    createSpotifyErrorDiv() {
        let message = 'Error while loading!\n\n'
        if (this.props.graphLoadingErrorMessage) {
            message += this.props.graphLoadingErrorMessage
        }

        return (
            <div className="page-wrapper">
                <div className="generic-description">{message}</div>
            </div>
        )
    }

    render() {
        if (this.props.graphLoading) {
            return this.createGraphLoadingDiv()
        }

        if (this.props.isGraphLoadingError) {
            return this.createSpotifyErrorDiv()
        }

        const clusterIsChosen = this.props.chosenCluster !== undefined
        const display = clusterIsChosen ? "none" : ""
        const w = this.state.dimensions ? this.state.dimensions.w : 500
        const h = this.state.dimensions ? this.state.dimensions.h : 500

        return (
            <div className="wrapper" ref={this.wrapperRef}>
                <TransformWrapper
                    defaultScale={1}
                    defaultPositionX={1}
                    defaultPositionY={1}
                    options={{
                        maxScale: 4,
                        limitToWrapper: true,
                    }}
                    onWheelStop={(e: any) => { this.setState({ currentScale: e.scale }) }}
                >
                    {({
                        zoomIn,
                        zoomOut,
                        setTransform,
                        ...rest
                    }: any) => (
                            <React.Fragment>
                                {
                                    clusterIsChosen &&
                                    <ChosenCluster key={this.props.chosenCluster.id} chosenCluster={this.props.chosenCluster} />
                                }
                                <TransformComponent>
                                    <div className="box" ref={this.boxRef}
                                        style={{ width: w, height: h, display: display }}
                                    >
                                        {
                                            this.state.dimensions &&
                                            this.createEdges()
                                        }
                                        {
                                            this.state.dimensions &&
                                            this.createNodes()
                                        }
                                    </div>
                                </TransformComponent>
                            </React.Fragment>
                        )}
                </TransformWrapper>
            </div>
        )
    }
}

const mapStateToProps = (state: State) => {
    return {
        graphLoaded: state.graphLoaded,
        graphLoading: state.graphLoading,
        isGraphLoadingError: state.isGraphLoadingError,
        graphLoadingMessage: state.graphLoadingMessage,
        graphLoadingErrorMessage: state.graphLoadingErrorMessage,
        graphLoadingRelatedArtistsProgress: state.graphLoadingRelatedArtistsProgress,
        graph: state.graph,
        chosenCluster: state.chosenCluster,

        accessToken: state.accessToken,
        sp: state.spotifyWebApiObject,

        isGraphFromFileLoaded: state.isGraphFromFileLoaded
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        setGraph: setGraph,
        graphFromFileIsLoaded: (isLoaded: boolean) => dispatch(graphFromFileIsLoaded(isLoaded)),
        loadGraph: (accessToken: string, sp: SpotifyWebApi.SpotifyWebApiJs, callback: Function) => dispatch(loadGraph(accessToken, sp, callback))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DraggableCanvas)
