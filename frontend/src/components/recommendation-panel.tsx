import React from 'react'
import { connect } from 'react-redux'
import { BsQuestion } from 'react-icons/bs'
import { State, ArtistSimplified } from '../store/types'
import { setChosenArtistDataAll, updateNotFavouriteArtists } from '../store/actions'
import { ArtistFull } from '../store/types'
import COLORS from '../methods/colors'
import { getAllArtistsFull } from '../methods/spotify-methods'

import CSS from 'csstype'
import './cluster-node.css'
import './artist-node.css'
import './recommendation-panel.css'
import SpotifyWebApi from 'spotify-web-api-js'
import { isUndefined } from 'util'

interface RecommendationPanelProps {
    clusterArtistsIds: string[],
    artistDict: {[x: string]: ArtistFull},
    sp: SpotifyWebApi.SpotifyWebApiJs,
    setChosenArtistDataAll: typeof setChosenArtistDataAll,
    updateNotFavouriteArtists: typeof updateNotFavouriteArtists,
    accessToken?: string,
    chosenArtist?: ArtistFull
}

interface RecommendationPanelState {
    artistsToRecommend: ArtistFull[]
    questionMarkHover: boolean
}

interface Counter {
    [id: string]: {count: number, related: string[]}
}

interface CounterListObject {
    id: string
    count: number
    related: string[]
}

/**
 * 
 */
class RecommendationPanel extends React.Component<RecommendationPanelProps, RecommendationPanelState> {
    constructor(props: RecommendationPanelProps) {
        super(props);
        this.state = {
            questionMarkHover: false,
            artistsToRecommend: []
        }
        this.getArtistsToRecommend()
    }

    getArtistsToRecommend() {
        const counter = this.getCounter(this.props.clusterArtistsIds.map(id => this.props.artistDict[id]))
        console.debug('COUNTER', counter)        
        const topIds = this.getTopIds(counter)

        if (topIds.length === 0) return <div></div>

        const fetchArtistsPromise = getAllArtistsFull(this.props.sp, topIds)
        Promise.resolve(fetchArtistsPromise).then((result) => {
            const finalArtists: ArtistFull[] = []
            
            for (let i = 0; i < result.length; i++) {
                const aFull = result[i];
                const artistImage = aFull.images && aFull.images.length > 0 ? aFull.images[aFull.images.length - 1].url : ''
                finalArtists.push({
                    genres: aFull.genres,
                    id: aFull.id,
                    name: aFull.name,
                    relatedArtistsIds: counter[aFull.id].related,
                    image: artistImage,
                    isFollowed: false,
                    likedSongs: [],
                    score: 1
                })
            }

            this.setState({artistsToRecommend: finalArtists})
        })
    }

    getCounter(artists: ArtistFull[]) {
        const counter: Counter = {}
        for (let i = 0; i < artists.length; i++) {
            const a = artists[i];
            for (let j = 0; j < a.relatedArtistsIds.length; j++) {
                const relID = a.relatedArtistsIds[j];
                if (!(relID in this.props.artistDict)) {
                    if (relID in counter) {
                        const current = counter[relID]
                        counter[relID] = {count: current.count + 1, related: current.related.concat([a.id])}
                    } else {
                        counter[relID] = {count: 1, related: [a.id]}
                    }
                }
            }
        }
        return counter
    }

    getTopIds(counter: Counter): string[] {
        const maximumRecommended = 15
        const minimumRelated = 2

        const counterKeys = Object.keys(counter)
        const counterValues = Object.values(counter)
        const toSort: CounterListObject[] = []
        for (let i = 0; i < counterKeys.length; i++) {
            const key = counterKeys[i]
            const val = counterValues[i]
            toSort.push({id: key, count: val.count, related: val.related})
        }
        toSort.sort((a,b) => b.count - a.count)

        const artistsToRecommend = []
        for (let i = 0; i < maximumRecommended; i++) {
            if (i >= toSort.length) break
            if (toSort[i].count < minimumRelated) break

            artistsToRecommend.push(toSort[i])
        }
        return artistsToRecommend.map(o => o.id)
    }

    componentDidMount() {

    }

    componentDidUpdate() {

    }

    createRecommendArtistDiv(artistFull: ArtistFull) {
        let color = COLORS.darkAzureBlue
        if (this.props.chosenArtist && this.props.chosenArtist.id === artistFull.id) {
            color = COLORS.pink
        }
        
        const nodeWidth = 60
        const nodeHeight = 60
        const padding = 2

        const wrapperStyle: CSS.Properties = {
            backgroundColor: color,
            display:"flex", 
            padding:`${padding}px`,
            height:`${nodeHeight+2*padding}px`,
            borderRadius: "10px",
            margin: "0px 1px 0px 1px"
        }

        const artistImageDivStyle: CSS.Properties = {
            backgroundImage: `url(${artistFull.image})`,
            backgroundSize: `${nodeHeight}px ${nodeWidth}px`,
            width: `${nodeWidth}px`,
            height: `${nodeHeight}px`,
            borderRadius: "10px",
            alignSelf: "center",
            margin: "2px"
        }

        const token = isUndefined(this.props.accessToken) ? '' : this.props.accessToken
        return (
            <div style={wrapperStyle}
                title={artistFull.name}
                onClick={() => {
                    this.props.updateNotFavouriteArtists([artistFull])
                    this.props.setChosenArtistDataAll(artistFull, token)

                }}
            >
                <div style={{ ...artistImageDivStyle, borderColor: COLORS.lightAzureBlue }}>
                    {/* <div style={artistImageDivStyle} /> */}
                </div>
            </div>
        )
    }

    changeHover() {
        this.setState({questionMarkHover: !this.state.questionMarkHover})
    }

    render() {
        if (this.state.artistsToRecommend.length === 0) {
            return (
                <div className="recommendation-panel">
                    <div style={{flex: "1 1 auto", textAlign:"center"}}>
                        <p style={{margin:"0px"}}>No artists to recommend</p>
                        <p>Either there are not enough artists in this cluster, or they are not really related.</p>
                    </div>
                </div>
            )
        }

        const divs = this.state.artistsToRecommend.map(a => this.createRecommendArtistDiv(a))
        const showHover = this.state.questionMarkHover ? {} : {display:'none'}
        return (
            <div>
                <div className="you-might-like-description" style={{...showHover}}>
                    <p>Based on the artists in the current cluster, you could like the following artists.</p>
                    <p>After some of the artists is clicked, their related artists are highlighted yellow in the graph.</p>
                </div>
                <div className="you-might-like">
                    <b>You might also like:</b>
                    <BsQuestion 
                        onMouseEnter={() => this.changeHover()} 
                        onMouseLeave={() => this.changeHover()} 
                        size={25} />
                </div>
                <div className="recommendation-panel">
                    {divs}
                </div>
            </div>
        )
    }
}

export default connect(
    (state: State) => ({
        artistDict: state.graph.artistDict,
        sp: state.spotifyWebApiObject,
        accessToken: state.accessToken,
        chosenArtist: state.chosenArtist
    }),
    (dispatch: any) => ({
        setChosenArtistDataAll: (chosenArtist: ArtistFull, accessToken: string) => dispatch(setChosenArtistDataAll(chosenArtist, accessToken)),
        updateNotFavouriteArtists: (artistsToUpdate: ArtistSimplified[]) => dispatch(updateNotFavouriteArtists(artistsToUpdate))
    })
)(RecommendationPanel)
