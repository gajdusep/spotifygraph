import React from 'react'
import Draggable, { DraggableEvent } from 'react-draggable'
import CSS from 'csstype'
import { connect } from 'react-redux'

import { State } from '../store/types'
import { setChosenArtistDataAll } from '../store/actions'
import { ArtistFull } from '../store/types'
import COLORS from '../methods/colors'

import './cluster-node.css'
import './artist-node.css'

interface ArtistNodeProps {
    /** Artist to be displayed in the node */
    artist: ArtistFull
    /** Initial X coordinate */
    positionX: number
    /** Initial Y coordinate */
    positionY: number
    /** Minimal score for the artist to be shown */
    minimumScore: number
    /** ArtistFull object of the chosen artist */
    chosenArtist?: ArtistFull
    /** Artist dictionary */
    artistDict: {[id: string]: ArtistFull}
    /** Spotify API access token */
    accessToken?: string
    /** Callback for position change - needed to inform edges about node position change */
    positionChangedCallback: Function
    /** Function to set chosen artist in the State */
    setChosenArtistDataAllFunc: typeof setChosenArtistDataAll
}

interface ArtistNodeState {
    currentPositionX: number
    currentPositionY: number
    dragging: boolean
}

/**
 * Artist node in the Inside cluster graph 
 */
class ArtistNode extends React.Component<ArtistNodeProps, ArtistNodeState> {
    constructor(props: ArtistNodeProps) {
        super(props);
        this.state = {
            currentPositionX: props.positionX,
            currentPositionY: props.positionY,
            dragging: false,
        }
        this.onDrag = this.onDrag.bind(this)
        this.onStop = this.onStop.bind(this)
    }

    onDrag(e: DraggableEvent, ui: any) {
        e.stopPropagation(); e.preventDefault();
        this.setState({ dragging: true })
        const newCurrX = this.state.currentPositionX + ui.deltaX
        const newCurrY = this.state.currentPositionY + ui.deltaY
        this.setState({ currentPositionX: newCurrX, currentPositionY: newCurrY })
        this.props.positionChangedCallback(newCurrX, newCurrY, this.props.artist.id)
    }

    onStop(e: DraggableEvent) {
        e.stopPropagation(); e.preventDefault();
        const dragging = this.state.dragging;
        this.setState({ dragging: false })
        if (!dragging) {
            this.handleClick();
        }
    }

    handleClick() {
        if (this.props.accessToken) {
            if (this.props.chosenArtist && this.props.chosenArtist.id === this.props.artist.id) {
                return
            }
            else {
                this.props.setChosenArtistDataAllFunc(this.props.artist, this.props.accessToken);
            }
        }
    }

    render() {
        let color = COLORS.lightAzureBlue
        if (this.props.chosenArtist) {
            if (this.props.chosenArtist.id === this.props.artist.id) {
                color = COLORS.pink
            }
            if (!(this.props.chosenArtist.id in this.props.artistDict)) {
                if (this.props.chosenArtist.relatedArtistsIds.indexOf(this.props.artist.id) !== -1) {
                    color = COLORS.orange
                }
            }
            
        }

        const nodeWidth = 60
        const nodeHeight = 60

        const artistImageDivStyle: CSS.Properties = {
            backgroundImage: `url(${this.props.artist.image})`,
            backgroundSize: `${nodeHeight}px ${nodeWidth}px`,
            width: `${nodeWidth}px`,
            height: `${nodeHeight}px`,
            borderRadius: "10px",
            alignSelf: "center"
        }

        if (this.props.artist.score < this.props.minimumScore) {
            return <div></div>
        }

        return (
            <Draggable
                key={this.props.artist.id}
                bounds="parent"
                defaultPosition={{ x: this.props.positionX, y: this.props.positionY }}
                onDrag={this.onDrag}
                onStop={this.onStop}
            >
                <div className="artist-node-outer-wrapper">
                    <div style={{ ...artistImageDivStyle, backgroundColor: COLORS.lightAzureBlue }}>
                        <div style={artistImageDivStyle} />
                    </div>
                    <div className="artist-node-artist-name-div" style={{ backgroundColor: color }}>
                        {this.props.artist.name}
                    </div>
                </div>
            </Draggable>
        )
    }
}

export default connect(
    (state: State) => ({
        accessToken: state.accessToken,
        chosenArtist: state.chosenArtist,
        artistDict: state.graph.artistDict
    }),
    (dispatch: any) => ({
        setChosenArtistDataAllFunc: (chosenArtist: ArtistFull, accessToken: string) => dispatch(setChosenArtistDataAll(chosenArtist, accessToken))
    })
)(ArtistNode)
