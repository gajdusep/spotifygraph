import React from 'react'
import { Provider } from 'react-redux'
import { createStoreInstance } from './store/reducers'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import WelcomePage from './components/welcome-page'
import './App.css'
import LoginPage from './components/login-page'
import MyGraphPage from './components/my-graph-page';

export const storeInstance = createStoreInstance()

const App: React.FC = () => {
    return (
        <Provider store={storeInstance}>
            <BrowserRouter basename={process.env.PUBLIC_URL}>
                <Switch>
                    <Route path="/login" component={LoginPage} />
                    <Route path="/mygraph" component={MyGraphPage} />
                    <Route path="/" component={WelcomePage} />
                </Switch>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
