import { produce } from 'immer'
import { ActionWrapper, ThunkActionWrapper, State, ArtistFull, Cluster, Graph, SongFull, ChosenArtistData, ArtistSimplified } from './types'
import SpotifyWebApi from 'spotify-web-api-js'
import { isUndefined } from 'util'
import { fetchAllDataFromSpotify, songFromJSONData } from '../methods/spotify-methods'
import { isRight } from 'fp-ts/lib/Either'
import { GraphType } from '../store/types'
// import { PathReporter } from 'io-ts/lib/PathReporter'

export const setAccessToken = (accessToken: string): ActionWrapper<string> => ({
    type: 'SET_ACCESS_TOKEN',
    payload: accessToken,
    reducer: produce((state: State) => {
        state.accessToken = accessToken
        state.userLoggedIn = true
    })
})

export const setUserLoggedIn = (): ActionWrapper<void> => ({
    type: 'LOG_IN',
    reducer: produce((state: State) => {
        state.userLoggedIn = true
    })
})

export const logOut = (): ActionWrapper<void> => ({
    type: 'LOG_OUT',
    reducer: produce((state: State) => {
        state.accessToken = undefined
        state.userLoggedIn = false
    })
})

export const setChosenArtist = (artist: ArtistFull): ActionWrapper<ArtistFull> => ({
    type: 'SET_CHOSEN_ARTIST',
    payload: artist,
    reducer: produce((state: State) => {
        state.chosenArtist = artist
    })
})

export const setChosenCluster = (cluster: Cluster): ActionWrapper<Cluster> => ({
    type: 'SET_CHOSEN_CLUSTER',
    payload: cluster,
    reducer: produce((state: State) => {
        state.chosenCluster = cluster
    })
})

export const clearChosenClusterAndArtist = (): ActionWrapper<void> => ({
    type: 'CLEAR_CHOSEN_CLUSTER_ARTIST',
    reducer: produce((state: State) => {
        state.chosenCluster = undefined
        state.chosenArtist = undefined
    })
})

export const closeChosenClusterGraph = (): ActionWrapper<void> => ({
    type: 'CLOSE_CHOSEN_CLUSTER_GRAPH',
    reducer: produce((state: State) => {
        state.chosenCluster = undefined
    })
})

export const closeChosenArtist = (): ActionWrapper<void> => ({
    type: 'CLOSE_CHOSEN_ARTIST',
    reducer: produce((state: State) => {
        state.chosenArtist = undefined
    })
})

export const setGraph = (graph: Graph): ActionWrapper<Graph> => ({
    type: 'SET_GRAPH',
    payload: graph,
    reducer: produce((state: State) => {
        state.graph = graph
    })
})

export const setGraphArtists = (artists: ArtistFull[]): ActionWrapper<ArtistFull[]> => ({
    type: 'SET_GRAPH_ARTISTS',
    payload: artists,
    reducer: produce((state: State) => {
        state.graphLoading = false
        state.graphLoaded = true
    })
})

export const graphLoading = (): ActionWrapper<void> => ({
    type: 'GRAPH_LOADING',
    reducer: produce((state: State) => {
        state.graphLoading = true
    })
})

export const setGraphLoadingMessage = (message: string): ActionWrapper<string> => ({
    type: 'SET_GRAPH_LOADING_MESSAGE',
    reducer: produce((state: State) => {
        state.graphLoadingMessage = message
    })
})

export const setGraphLoadingErrorMessage = (errorMessage: string): ActionWrapper<string> => ({
    type: 'SET_GRAPH_LOADING_ERROR_MESSAGE',
    reducer: produce((state: State) => {
        state.graphLoadingErrorMessage = errorMessage
    })
})

export const setGraphLoadingProgress = (progress: number): ActionWrapper<number> => ({
    type: 'SET_GRAPH_LOADING_MESSAGE',
    reducer: produce((state: State) => {
        state.graphLoadingRelatedArtistsProgress = progress
    })
})

export const graphLoadingError = (): ActionWrapper<void> => ({
    type: 'GRAPH_LOADING_ERROR',
    reducer: produce((state: State) => {
        state.graphLoading = false
        state.isGraphLoadingError = true
    })
})

export const graphLoadingSuccess = (): ActionWrapper<void> => ({
    type: 'GRAPH_LOADING_SUCCESS',
    reducer: produce((state: State) => {
        state.graphLoading = false
        state.graphLoaded = true
        state.isGraphLoadingError = false
    })
})

export const setChosenArtistError = (error: string): ActionWrapper<string> => ({
    type: 'SET_CHOSEN_ARTIST_ERROR',
    payload: error,
    reducer: produce((state: State) => {
        state.chosenArtistError = error
    })
})

export const setChosenArtistLoading = (loading: boolean): ActionWrapper<boolean> => ({
    type: 'SET_CHOSEN_ARTIST_LOADING',
    payload: loading,
    reducer: produce((state: State) => {
        state.chosenArtistLoading = loading
    })
})

export const setChosenArtistData = (artistData: ChosenArtistData, chosenArtist: ArtistFull): ActionWrapper<ChosenArtistData> => ({
    type: 'SET_CHOSEN_ARTIST_DATA',
    payload: artistData,
    reducer: produce((state: State) => {
        state.chosenArtistData = artistData
        state.chosenArtist = chosenArtist
    })
})

export const setSongToPlay = (song: SongFull): ActionWrapper<SongFull> => ({
    type: 'SET_SONG_TO_PLAY',
    payload: song,
    reducer: produce((state: State) => {
        if (!!!state.songToPlay || state.songToPlay.id !== song.id) {
            state.songToPlay = song
            state.paused = false
        }
    })
})

export const setPaused = (paused: boolean): ActionWrapper<boolean> => ({
    type: 'SET_PAUSED',
    payload: paused,
    reducer: produce((state: State) => {
        state.paused = paused
    })
})

export const setSpotifyApi = (accessToken: string): ActionWrapper<string> => ({
    type: 'SET_SPOTIFY_API',
    payload: accessToken,
    reducer: produce((state: State) => {
        try {
            const sApi = state.spotifyWebApiObject
            sApi.setAccessToken(accessToken)
            state.spotifyWebApiObject = sApi
        } catch (error) {
            console.debug(error)
        }
        console.debug('SET SPOTIFY API')
    })
})

/**
 * Updates not favourite artists dictionary
 * @param artistsToUpdate list of artists to add 
 */
export const updateNotFavouriteArtists = (artistsToUpdate: ArtistSimplified[]): ActionWrapper<any> => ({
    type: 'UPDATE_NOT_FAVOURITE_ARTISTS',
    payload: artistsToUpdate,
    reducer: produce((state: State) => {
        const currentNotFavouriteDict = state.nonFavouriteArtistDict
        const favouriteDict = state.graph.artistDict

        for (let i = 0; i < artistsToUpdate.length; i++) {
            const element = artistsToUpdate[i];
            if (element.id in favouriteDict) {
                // don't add
            }
            else {
                currentNotFavouriteDict[element.id] = element
            }
        }
        state.nonFavouriteArtistDict = currentNotFavouriteDict
    })
})

const artistsFromSongJSON = (element: SpotifyApi.TrackObjectFull, currentArtistId: string): ArtistSimplified[] => {
    const toRet: ArtistSimplified[] = element.artists.reduce((result: ArtistSimplified[], a: SpotifyApi.ArtistObjectSimplified) => {
        if (a.id !== currentArtistId) result.push({ id: a.id, name: a.name })
        return result
    }, [])
    return toRet
}

/**
 * Sets a chosen artist and prepares his songs - liked by the user and not liked by the user.
 * @param chosenArtist `ArtistFull` object 
 * @param accessToken Spotify API access token
 */
export const setChosenArtistDataAll = (
    chosenArtist: ArtistFull,
    accessToken: string
): ThunkActionWrapper<void> => async (dispatch): Promise<void> => {
    if (isUndefined(accessToken) || accessToken === null || accessToken === "") {
        dispatch(setChosenArtistError('The token was not loaded, cannot obtain the artist data.'))
        return
    }

    dispatch(setChosenArtist(chosenArtist))
    dispatch(setChosenArtistLoading(true))
    let s = new SpotifyWebApi();
    s.setAccessToken(accessToken)

    const likedSongs: SongFull[] = []
    const notLikedSongs: SongFull[] = []
    let artistToUpdate: ArtistSimplified[] = []

    const areSongsEqual = (s1: SongFull, s2: SongFull) => {
        return (s1.id === s2.id || s1.name === s2.name)
    }

    s.getArtistTopTracks(chosenArtist.id, 'CZ')
        .then(topTracks => {
            for (let i = 0; i < topTracks.tracks.length; i++) {
                const currArtistToUpdate = artistsFromSongJSON(topTracks.tracks[i], chosenArtist.id)
                artistToUpdate = artistToUpdate.concat(currArtistToUpdate)
                const songToCheck = songFromJSONData(topTracks.tracks[i])
                const indexOfLiked = chosenArtist.likedSongs.findIndex(likedSong => areSongsEqual(songToCheck, likedSong))

                if (indexOfLiked === -1) {
                    notLikedSongs.push(songToCheck)
                }
                else {
                    if (songToCheck.previewLink === "") {
                        songToCheck.previewLink = chosenArtist.likedSongs[indexOfLiked].previewLink
                    }
                    likedSongs.push(songToCheck)
                }
            }

            for (let i = 0; i < chosenArtist.likedSongs.length; i++) {
                const element = chosenArtist.likedSongs[i];
                const indexOfLiked = likedSongs.findIndex(likedSong => areSongsEqual(element, likedSong))
                if (indexOfLiked === -1) {
                    likedSongs.push(element)
                }

            }

            dispatch(updateNotFavouriteArtists(artistToUpdate))
            dispatch(setChosenArtistData({ likedSongs: likedSongs, notLikedSongs: notLikedSongs }, chosenArtist))
        })
        .catch((error: string) => {
            console.debug(error)
            dispatch(setChosenArtistError('During the artist data fetching, the following error occured:' + error))
        })
        .finally(() => {
            dispatch(setChosenArtistLoading(false))
        })
}

/**
 * Load the data from backend
 * @param accessToken Spotify API access token
 * @param sp `SpotifyWebApiJs` object
 * @param callback callback to be called after the data is loaded (necessary to DraggableCanvas graph displaying)
 */
export const loadGraph = (accessToken: string, sp: SpotifyWebApi.SpotifyWebApiJs, callback: Function): ThunkActionWrapper<void> =>
    async (dispatch): Promise<void> => {
        dispatch(graphLoading())

        fetchAllDataFromSpotify(
            accessToken, sp,
            (message: string) => { dispatch(setGraphLoadingMessage(message)) },
            (progress: number) => { dispatch(setGraphLoadingProgress(progress)) },
            (errorMessage: string) => { dispatch(setGraphLoadingErrorMessage(errorMessage)) }
        )
            .then(g => dispatch(setGraph(g)))
            .then(() => callback())
            .then(() => dispatch(graphLoadingSuccess()))
            .catch((error) => {
                console.debug(error)
                dispatch(graphLoadingError())
            })
    }

export const graphFromFileIsLoaded = (isLoaded: boolean): ActionWrapper<boolean> => ({
    type: 'GRAPH_FROM_FILE_IS_LOADED',
    payload: isLoaded,
    reducer: produce((state: State) => {
        state.isGraphFromFileLoaded = isLoaded
    })
})

/**
 * Action to load graph from file.
 * @param jsonToParse graph obtained from the file
 */
export const loadGraphFromFile = (jsonToParse: Graph): ThunkActionWrapper<void> =>
    async (dispatch): Promise<void> => {
        // type check
        const result = GraphType.decode(jsonToParse)
        if (!!!isRight(result)) {
            // alert("SORRY, THE FILE IS CORRUPTED!\n\n" + PathReporter.report(result))
            alert("SORRY, THE FILE IS CORRUPTED!")
            return
        }

        for (let i = 0; i < jsonToParse.clusters.length; i++) {
            const cluster = jsonToParse.clusters[i];
            if (isUndefined(cluster.artistsIds) || cluster.artistsIds.length === 0) {
                cluster.artistsIds = Object.keys(cluster.artistsPositions)
            }
        }

        await Promise.resolve(
            dispatch(clearChosenClusterAndArtist())
        )
            .then(() => dispatch(graphLoading()))
            .then(() => dispatch(setGraph(jsonToParse)))
            .then(() => dispatch(graphFromFileIsLoaded(true)))
            .then(() => dispatch(graphLoadingSuccess()))
    }
