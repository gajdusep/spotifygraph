import { ThunkAction } from 'redux-thunk'
import SpotifyWebApi from 'spotify-web-api-js'
import * as t from 'io-ts'

function optional<RT extends t.Any>(
    type: RT,
    name: string = `${type.name} | undefined`
): t.UnionType<
    [RT, t.UndefinedType],
    t.TypeOf<RT> | undefined,
    t.OutputOf<RT> | undefined,
    t.InputOf<RT> | undefined
> {
    return t.union<[RT, t.UndefinedType]>([type, t.undefined], name);
}

export type StringDict<T> = {
    [id: string]: T
}

export type NumberDict<T> = {
    [id: number]: T
}


export const PositionType = t.type({
    x: t.number,
    y: t.number
})
export type Position = t.TypeOf<typeof PositionType>

export const SongFullType = t.type({
    artistsIds: t.array(t.string),
    externalUrl: t.string,
    id: t.string,
    name: t.string,
    previewLink: t.union([t.string, t.null])
})
export type SongFull = t.TypeOf<typeof SongFullType>

export const SongReducedType = t.type({
    id: t.string,
    name: t.string
})
export type SongReduced = t.TypeOf<typeof SongReducedType>

export const ArtistFullType = t.type({
    genres: t.array(t.string),
    id: t.string,
    image: t.string,
    isFollowed: t.boolean,
    likedSongs: t.array(SongFullType),
    name: t.string,
    relatedArtistsIds: t.array(t.string),
    score: t.number,
})
export type ArtistFull = t.TypeOf<typeof ArtistFullType>

export const ArtistReducedType = t.type({
    genres: t.array(t.string),
    id: t.string,
    relatedArtistsIds: t.array(t.string)
})
export type ArtistReduced = t.TypeOf<typeof ArtistReducedType>

export const ArtistSimplifiedType = t.type({
    id: t.string,
    name: t.string
})
export type ArtistSimplified = t.TypeOf<typeof ArtistSimplifiedType>

export const ArtistEdgeType = t.type({
    id1: t.string,
    id2: t.string,
    weight: t.number
})
export type ArtistEdge = t.TypeOf<typeof ArtistEdgeType>

export const ClusterType = t.type({
    id: t.string,
    genres: t.array(t.string),
    artistsIds: t.array(t.string),
    artistsPositions: t.dictionary(t.string, PositionType),
    country: optional(t.string)
})
export type Cluster = t.TypeOf<typeof ClusterType>

export const ClusterEdgeType = t.type({
    id1: t.string,
    id2: t.string,
    weight: t.number,
})
export type ClusterEdge = t.TypeOf<typeof ClusterEdgeType>

export type ClusterSquare = {
    [clusterId: string]: Position
}

export const GraphType = t.type({
    clusters: t.array(ClusterType),
    clusterEdges: t.array(ClusterEdgeType),
    artistDict: t.dictionary(t.string, ArtistFullType),
    artistEdges: t.array(ArtistEdgeType),
    clusterSquares: t.array(t.dictionary(t.string, PositionType))
})
export type Graph = t.TypeOf<typeof GraphType>

export interface ChosenArtistData {
    notLikedSongs: SongFull[]
    likedSongs: SongFull[]
}

export interface State {
    chosenCluster?: Cluster

    chosenArtist?: ArtistFull
    chosenArtistData?: ChosenArtistData
    chosenArtistError?: string
    chosenArtistLoading: boolean

    graph: Graph

    // access variables
    userLoggedIn: boolean
    accessToken?: string

    // the song being played
    songToPlay?: SongFull
    nonFavouriteArtistDict: StringDict<ArtistSimplified>

    // the state indicating the graph loading process
    graphLoaded: boolean
    graphLoading: boolean
    
    isGraphLoadingError: boolean
    graphLoadingErrorMessage?: string

    graphLoadingMessage: string
    graphLoadingRelatedArtistsProgress: number

    isGraphFromFileLoaded: boolean

    // prepared spotify web api for requests:
    spotifyWebApiObject: SpotifyWebApi.SpotifyWebApiJs

    // player state
    paused: boolean
}

export interface ActionWrapper<Payload = undefined> {
    type: string
    payload?: Payload
    reducer: (state: State) => State
}

export type ThunkActionWrapper<T> = ThunkAction<Promise<T>, State, {}, ActionWrapper<any>>
