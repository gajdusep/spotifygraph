import { createStore, applyMiddleware, compose } from 'redux';
import { State, ActionWrapper } from './types';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import SpotifyWebApi from 'spotify-web-api-js'

const middleWare: any = [];
const loggerMiddleware = createLogger({
    predicate: (getState: Function, action: ActionWrapper) => process.env.NODE_ENV === 'development' && action.type !== 'SET_GRAPH_LOADING_MESSAGE',
    collapsed: (getState, action) => true
});
middleWare.push(thunk)
middleWare.push(loggerMiddleware)


const getInitialState = (): State => {
    return ({
        isGraphLoadingError: false,
        graphLoaded: false,
        graphLoading: false,
        chosenArtistLoading: false,
        userLoggedIn: false,
        graph: ({ clusters: [], clusterEdges: [], artistEdges: [], clusterSquares: [], artistDict: {} }),
        spotifyWebApiObject: new SpotifyWebApi(),
        graphLoadingRelatedArtistsProgress: 0,
        graphLoadingMessage: "",
        paused: false,
        isGraphFromFileLoaded: false,
        nonFavouriteArtistDict: {}
    })
}

const rootReducer = (
    state: State = getInitialState(),
    action: ActionWrapper<any>
) => {
    if (!action.reducer) return state
    else return action.reducer(state)
}

const createStoreInstance = () => createStore(
    rootReducer,
    getInitialState(),
    compose(
        applyMiddleware(...middleWare)
    ))

export { createStoreInstance }
