
mypy --ignore-missing-imports .

pylint --output-format=colorized --max-line-length=120 apps/graphapp/src/ \
    --disable=invalid-name,missing-function-docstring,missing-module-docstring,missing-class-docstring
