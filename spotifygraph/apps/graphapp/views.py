from django.http import HttpResponse, JsonResponse
from urllib import parse
import json
import traceback
import time
import os
from django.views.decorators.csrf import csrf_exempt

from apps.graphapp.src.user_connection import UserConnection
from apps.graphapp.src.edges_creator import EdgesCreator
from apps.graphapp.src.graph import Graph
from apps.graphapp.src.models import ArtistEdge, Artist


def logged_in(request):
    """
    request: path must include valid code for access-token request\\
    returns: HttpResponse \\
        200 - the request on Spotify API was successfull - access token in access-token header\\
        401 - if the request was sent from unknown source \\
    """    

    print('--- LOGGED IN ---')
    authorize_uri = os.environ.get('SPOTIFY_GRAPH_AUTHORIZE_URI', '')
    request_origin = request.META.get('HTTP_REFERER', '')

    url_should_be = '{uri.scheme}://{uri.netloc}/'.format(uri=parse.urlparse(authorize_uri))
    url_is = '{uri.scheme}://{uri.netloc}/'.format(uri=parse.urlparse(request_origin))
    
    # check the origin
    if (url_should_be != url_is):
        print('unauthorized. Url should be: {}, Url is: {}'.format(url_should_be, url_is))
        return HttpResponse('Unauthorized.', status=401)

    # parse the url, get code and state
    query = parse.parse_qs(parse.urlparse(request.get_full_path()).query)
    code = query.get('code', '')
    state = query.get('state', '')

    try:
        access_token = UserConnection.get_access_token(code, authorize_uri)
        user_id = UserConnection.get_user_info(access_token)
    except Exception as e:
        print('Unable to get the access token.', e)
        return HttpResponse('Unable to get the access-token.', status=500)

    response = HttpResponse()
    response['Access-Control-Expose-Headers'] = 'access-token'
    response["access-token"] = access_token
    return response


@csrf_exempt
def calculate_graph(request):
    """
    request: Calculates graph edges, clusters and positions of nodes in the graph. Body must contain artists dict. \\
    returns: JsonResponse\\
        { \\
            'artistEdges': edges with weigth between artists\\
            'clusters': cluster with information about artists and their positions\\
            'clusterEdges': \\
            'clusterSquares': positions of clusters\\
        } \\
        500 - if anything fails, {'success':false, 'message':'error message'} is returned
    """

    print('--- CALCULATE GRAPH ---')
    authorize_uri = os.environ.get('SPOTIFY_GRAPH_AUTHORIZE_URI', '')
    request_origin = request.META.get('HTTP_REFERER', '')

    url_should_be = '{uri.scheme}://{uri.netloc}/'.format(uri=parse.urlparse(authorize_uri))
    url_is = '{uri.scheme}://{uri.netloc}/'.format(uri=parse.urlparse(request_origin))
    
    # check the origin
    if (url_should_be != url_is):
        print('unauthorized. Url should be: {}, Url is: {}'.format(url_should_be, url_is))
        return HttpResponse('Unauthorized.', status=401)

    if request.method == 'POST':
        # get the parameter - artists dictionary Dict[str, Artist]
        artist_dict = json.loads(request.body)

        # check if the data is in supported format
        if not isinstance(artist_dict, dict):
            return JsonResponse({'success':'false', 'message':'Recieved data is not valid - it must be in the form Dict[str, ArtistInfo].'}, safe=False, status=500)
        if len(artist_dict.items()) == 0:
            return JsonResponse({'success':'false', 'message':'Recieved data is empty. It is necessary to like some song / follow artists.'}, safe=False, status=500)

        # get the necessary information of the given artist_dict
        try:
            artist_nodes: List[Artist] = list(map(lambda x: Artist(
                id=x['id'], name='', songs=[], genres=x['genres'], related_artists_ids=x['relatedArtistsIds']
            ), list(artist_dict.values())))
            artist_objects_dict: Dict[str, Artist] = {}
            for a in artist_nodes:
                artist_objects_dict[a.id] = a
        except KeyError as e:
            return JsonResponse({'success':'false', 'message':'The received artist data is not valid - check the artists objects.'}, safe=False, status=500)

        # create edges
        edges_creator = EdgesCreator()
        try:
            artist_edges = edges_creator.get_edges(artist_objects_dict)
        except Exception:
            return JsonResponse({'success':'false', 'message':'Unable to create edges - check the artists objects.'}, safe=False, status=500)
        artist_edges_jsons = list(map(lambda x: x.to_json(), artist_edges))
        
        # calculate the graph
        try:
            my_graph = Graph(artist_nodes, artist_edges)
            start = time.time()
            graph_json = my_graph.process()
            end = time.time()
            print('graph calculation: ', round(end - start, 3), 's')
        except Exception as e:
            print(traceback.format_exc())
            return JsonResponse({'success':'false', 'message':'Some serious problem happened during the graph calculation, please contact the developer.' + str(repr(e))}, safe=False, status=500)

        json_to_return = {
            'artistEdges': artist_edges_jsons,
            'clusters': graph_json['nodes'],
            'clusterEdges': graph_json['edges'],
            'clusterSquares': graph_json['positions']
        }

        return JsonResponse(json_to_return, safe=False)
 