from django.urls import path

from . import views

urlpatterns = [
    path('calculategraph/', views.calculate_graph, name='calculategraph'),
    path('logged_in/', views.logged_in, name='logged_in'),
]
