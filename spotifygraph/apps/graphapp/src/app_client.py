import json
import os
from typing import Tuple


class AppClient():
    """
    Wrapper for important env variables.
    """

    @staticmethod
    def get_client_id() -> str:
        return os.environ.get('SPOTIFY_CLIENT_ID', '')

    @staticmethod
    def get_client_secret() -> str:
        return os.environ.get('SPOTIFY_CLIENT_SECRET', '')

    @staticmethod
    def get_client_credentials() -> Tuple[str, str]:
        return AppClient.get_client_id(), AppClient.get_client_secret()
