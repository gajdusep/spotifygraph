from typing import Dict, List, Union
import math
import networkx as nx
import numpy as np
from scipy.spatial import Delaunay

from apps.graphapp.src.models import Position

class GrowingTree:

    def __init__(self, node_width):
        self.node_width = node_width
        super().__init__()

    def gtree(self, nx_positions: Dict[Union[str, int], Position], width, height, node_width, node_height):
        """
        nx_positions: dict of positions {0:[x1,y1], 1:[x2,y2]...}
        width:
        height:
        node_width: size of the node in pixels
        node_height:
        """

        points = np.array(list(map(lambda pos: [pos.x, pos.y], list(nx_positions.values()))))
        
        # create a graph with nodes as in the graph, edges made of Delaunay triangulation
        new_graph = nx.Graph()

        # translate the names of nodes..
        new_nx_positions: Dict[Union[str, int], Position] = {}
        index_point_id_dict = {}
        for i in range(len(nx_positions.items())):
            point_id, point_position = list(nx_positions.items())[i]
            new_nx_positions[point_id] = point_position
            new_graph.add_node(point_id, pos=(point_position.x, point_position.y))
            index_point_id_dict[i] = point_id

        # do the Delaunay triangulation
        def checkEqual(lst):
            if len(lst) == 0:
                return False
            return all(elem == lst[0] for elem in lst)

        if checkEqual(points[:, 1]):
            points[0, 1] = points[0, 1] + 0.01
        if checkEqual(points[:, 0]):
            points[0, 1] = points[0, 0] + 0.01

        tri = Delaunay(points, qhull_options="QJ")        

        # add edges from triangulation
        simplices = tri.simplices
        for i in range(len(simplices)):
            simpl = simplices[i]
            new_graph.add_edge(index_point_id_dict[simpl[0]], index_point_id_dict[simpl[1]], weight=self.dist(
                points[simpl[0]], points[simpl[1]]))
            new_graph.add_edge(index_point_id_dict[simpl[1]], index_point_id_dict[simpl[2]], weight=self.dist(
                points[simpl[1]], points[simpl[2]]))
            new_graph.add_edge(index_point_id_dict[simpl[0]], index_point_id_dict[simpl[2]], weight=self.dist(
                points[simpl[0]], points[simpl[2]]))

        # check is some node is overlapping
        to_update = False
        for (u, v, d) in new_graph.edges(data=True):
            if d['weight'] < self.node_width:
                to_update = True
                break
            else:
                pass

        # if all nodes are not overlapping, return the original positions
        if not to_update:
            return nx_positions
        else:
            pass

        # do the minimum spanning tree on the triangulation
        min_span_tree = nx.minimum_spanning_tree(new_graph)

        # prepare all the nodes positions: p'_r = p_r
        new_positions: Dict[Union[str, int], Position] = {}
        for key, val in new_nx_positions.items():
            new_positions[key] = val

        self.grow_at_node(
            index_point_id_dict[0], min_span_tree, None, nx_positions, new_positions)

        return new_positions

    def grow_at_node(self, node, tree, already_visited_neighbor, 
        old_positions: Dict[Union[str,int], Position], new_positions: Dict[Union[str,int], Position]):
        node_width = self.node_width
        for neighbor in tree.neighbors(node):
            if neighbor == already_visited_neighbor:
                continue

            # compute t
            x_diff = abs(old_positions[neighbor].x - old_positions[node].x)
            y_diff = abs(old_positions[neighbor].y - old_positions[node].y)

            if y_diff < 2*node_width and x_diff < 2*node_width:
                t = 2*node_width / max(y_diff, x_diff)
            else:
                t = 1

            potentialy_new_position = self.get_potentialy_new_position(new_positions, old_positions, node, neighbor, t)
            new_positions[neighbor] = potentialy_new_position
            self.grow_at_node(neighbor, tree, node, old_positions, new_positions)

    def get_potentialy_new_position(self, new_positions, old_positions, node:str, neigbhbor:str, t: float) -> Position:
        pot_x = np.round(new_positions[node].x + t * (old_positions[neigbhbor].x - old_positions[node].x), 2)
        pot_y = np.round(new_positions[node].y + t * (old_positions[neigbhbor].y - old_positions[node].y), 2)        
        return Position(pot_x, pot_y)

    def dist(self, a, b):
        x_a = a[0]
        y_a = a[1]
        x_b = b[0]
        y_b = b[1]

        x_2 = math.pow((x_a - x_b), 2)
        y_2 = math.pow((y_a - y_b), 2)
        return math.sqrt(x_2 + y_2)
