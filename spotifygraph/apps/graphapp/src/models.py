from typing import List, Set, Tuple, Dict, Union


class Song:
    def __init__(self, id: str = "unknown", name: str = "unknown", artists_ids: List[str] = []):
        self.id = id
        self.name = name
        self.artist_ids = artists_ids

    def __str__(self):
        return 'Song({}, {})'.format(self.id, self.name)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, value):
        return self.id == value.id

    def __hash__(self):
        return hash(self.id)


class Artist:
    def __init__(self, id: str, related_artists_ids: List[str], name: str = '',
                 songs: List[Song] = [], genres: List[str] = [], popularity: float = 0,
                 users_popularity=0, image_link: str = "", is_followed: bool = False,
                 
                 ):
        self.genres: List[str] = genres
        self.name: str = name
        self.image_link: str = image_link
        self.popularity: float = popularity
        self.users_popularity: float = users_popularity
        self.id: str = id
        self.songs: List[Song] = songs
        self.is_followed: bool = is_followed
        self.related_artists_ids: List[str] = related_artists_ids

    def __str__(self):
        return 'Artist({}, {})'.format(self.id, self.name)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, value):
        return self.id == value.id

    def __hash__(self):
        return hash(self.id)

    def to_json(self):
        return {
            'genres': self.genres,
            'id': self.id,
            'image': self.image_link,
            'isFollowed': self.is_followed,
            'likedSongs': self.songs,
            'name': self.name,
            'score': self.popularity,
            'relatedArtistsIds':  self.related_artists_ids
        }


class ArtistEdge:

    def __init__(self, id1: str, id2: str, weight: float):
        self.id1 = id1
        self.id2 = id2
        self.weight = weight

    def __str__(self):
        return 'ArtistEdge({}, {}: {})'.format(self.id1, self.id2, self.weight)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, value):
        return (self.id1 == value.id1 and self.id2 == value.id2) or \
               (self.id1 == value.id2 and self.id2 == value.id1)

    def __hash__(self):
        return hash(self.id1 + self.id2)

    def to_json(self):
        return {'id1': self.id1, 'id2': self.id2, 'weight': self.weight}


class ClusterEdge:
    def __init__(self, cluster_1: int, cluster_2: int, weight=1):
        self.cluster_1 = cluster_1
        self.cluster_2 = cluster_2
        self.weight = weight

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '{}---{}: {}    '.format(self.cluster_1, self.cluster_2, self.weight)

    def to_json(self):
        return {
            "id1": str(self.cluster_1),
            "id2": str(self.cluster_2),
            "weight": self.weight
        }


class Position:
    def __init__(self, x: int, y: int):
        self.x: int = x
        self.y: int = y
        
    def to_json(self):
        return {
            'x': self.x,
            'y': self.y
        }

class ClusterNode:
    def __init__(self, id: int, genres: List[str], artists_positions_dict: Dict[str, Position], country: str = ''):
        self.id = id
        self.genres = genres
        self.artists_positions_dict = artists_positions_dict
        self.country = country

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return str(self.to_json())

    def to_json(self):
        artist_positions_json = {}
        for artist_id, position in self.artists_positions_dict.items():
            artist_positions_json[artist_id] = position.to_json()
        return {
            "id": str(self.id),
            "genres": self.genres,
            "artistsPositions": artist_positions_json,
            "country": self.country
        }
