from typing import List, Set, Dict, Tuple
from apps.graphapp.src.models import Artist, ArtistEdge


class EdgesCreator():
    """
    Wrapper around `get_edges` method.
    """

    def __init__(self, genres_weight=0.2, related_artists_weight=1):
        """
        genres_weight: number, the weight that is given to every genre that two artists have in common \\
        related_artists_weight: number, the weight that is given to every one-directional connection given by `related_artists`
        """
        self.genre_weight = genres_weight
        self.related_artist_weight = related_artists_weight

    def get_edges(self, artist_dict: Dict[str, Artist]) -> List[ArtistEdge]:
        """
        `ArtistEdge`'s weight is calculated as following: \\
            number_of_genres_in_common*genres_weight + (is_1_related_to_2 + is_2_related_to_1)*related_artists_weight\\
        artists_dict: Dict[artist id, Artist object]\\
        returns: list of calculated edges.
        """

        print('GETTING EDGES')

        artists: List[Artist] = list(artist_dict.values())
        artists_set = set(artist_dict.keys())

        dict_of_dicts: Dict[str, Dict[str, float]] = {}

        for artist in artists:
            artists_id = artist.id
            related_artists_ids = artist.related_artists_ids

            related_artists_ids_set: Set[str] = set(related_artists_ids)
            intersection: Set[str] = artists_set.intersection(
                related_artists_ids_set)

            # add pair to set of pairs
            for intersect in intersection:

                if artists_id < intersect:
                    first = artists_id
                    second = intersect
                else:
                    first = intersect
                    second = artists_id

                if first in dict_of_dicts and second in dict_of_dicts[first]:
                    dict_of_dicts[first][second] += self.related_artist_weight
                elif first in dict_of_dicts:
                    dict_of_dicts[first][second] = self.related_artist_weight
                else:
                    dict_of_dicts[first] = {second: self.related_artist_weight}

        # add the genre weight
        for i in range(len(artists)):
            for j in range(len(artists)):
                artist1 = artists[i]
                artist2 = artists[j]
                if artist1.id < artist2.id:
                    first = artist1.id
                    second = artist2.id

                    genres1 = set(artist1.genres)
                    genres2 = set(artist2.genres)
                    len_intersection = len(
                        genres1 & genres2) * self.genre_weight

                    if len_intersection > 0:
                        if first in dict_of_dicts and second in dict_of_dicts[first]:
                            dict_of_dicts[first][second] += len_intersection
                        elif first in dict_of_dicts:
                            dict_of_dicts[first][second] = len_intersection
                        else:
                            dict_of_dicts[first] = {second: len_intersection}

        edges = []
        for key1 in dict_of_dicts:
            for key2 in dict_of_dicts[key1]:
                edges.append([key1, key2, dict_of_dicts[key1][key2]])

        artist_edges = list(map(lambda x: ArtistEdge(
            id1=x[0], id2=x[1], weight=x[2]), edges))

        return artist_edges
