from typing import Dict, List, Union
import networkx as nx
import numpy as np

from apps.graphapp.src.models import Position
from apps.graphapp.src.growing_tree import GrowingTree


class GraphVisualizer:
    """
    Wrapper with graph layout algorithms.
    """

    def __init__(self, graph_width: int, node_width: float):
        self.graph_width = graph_width
        self.node_width = node_width

    def get_basic_positions(self, graph_to_visualize: nx.Graph) -> Dict[Union[str, int], Position]:
        # positions = nx.drawing.nx_agraph.graphviz_layout(graph_to_visualize, prog='sfdp')
        # positions = nx.drawing.nx_agraph.graphviz_layout(graph_to_visualize, prog='neato')
        # positions = nx.drawing.kamada_kawai_layout(graph_to_visualize, weight=None)
        positions = nx.spring_layout(graph_to_visualize, weight='weight')
        for node, position in positions.items():
            positions[node] = Position(position[0], position[1])
        return positions

    def visualize_clusters_graph(self, cluster_nodes, cluster_edges):
        if len(cluster_nodes) < 1:
            return None, []

        induced_graph = nx.Graph()
        for node in cluster_nodes:
            induced_graph.add_node(node.id, width=10.0, height=10.0)
        for edge in cluster_edges:
            induced_graph.add_edge(edge.cluster_1, edge.cluster_2)
            induced_graph[edge.cluster_1][edge.cluster_2]['weight'] = edge.weight

        return self.visualize(induced_graph)

    def visualize(self, graph_to_visualize: nx.Graph):
        # get the positions according to some layout algorithm
        positions = self.get_basic_positions(graph_to_visualize)

        if len(positions) <= 3:
            self.normalize_positions(positions, 1, 1)
            return None, positions

        # initialize growing tree:
        growing_tree = GrowingTree(self.node_width)

        # NORMALIZE WIDTH, BUT HEIGHT A LITTLE BIT SMALLER..
        self.normalize_positions(positions, self.graph_width, self.graph_width)

        new_positions = growing_tree.gtree(positions, 0, 0, 0, 0)
        self.normalize_positions(
            new_positions, self.graph_width, self.graph_width/1)

        for i in range(10):
            new_positions_2 = growing_tree.gtree(new_positions, 0, 0, 0, 0)
            if new_positions_2 == new_positions:
                break
            else:
                new_positions = new_positions_2

        new_y_normalize = max(
            [p.y for p in new_positions.values()]) / self.graph_width
        self.normalize_positions(new_positions, 1, 1)

        return graph_to_visualize, new_positions

    def normalize_positions(self, positions: Dict[Union[str, int], Position], x_normalize, y_normalize):
        """Edits positions dict to minimal values be 0 and maximal values 1

        positions: dict as returned from networkx algorithm:
            {
                id: array([x,y])
            }
        """

        positions_values = positions.values()
        if len(positions_values) == 0:
            return

        min_x = min(positions_values, key=lambda pos: pos.x).x
        max_x = max(positions_values, key=lambda pos: pos.x).x
        min_y = min(positions_values, key=lambda pos: pos.y).y
        max_y = max(positions_values, key=lambda pos: pos.y).y

        def compute_new_val(min_val, max_val, val, to_normalize):
            if to_normalize == None:
                shift = -min_val
                return shift + val
            elif max_val - min_val == 0:
                return 0.5
            else:
                shift = -min_val
                multipl = to_normalize / (max_val - min_val)
                return (shift + val) * multipl

        for id, pos in positions.items():
            new_x = compute_new_val(min_x, max_x, pos.x, x_normalize)
            new_y = compute_new_val(min_y, max_y, pos.y, y_normalize)
            positions[id].x = round(new_x, 2)
            positions[id].y = round(new_y, 2)
