
import base64
from urllib import parse
import requests

from apps.graphapp.src.app_client import AppClient


class UserConnection():
    @staticmethod
    def get_access_token(code: str, authorize_uri: str) -> str:
        client_id, client_secret = AppClient.get_client_credentials()
        client_string = client_id + ':' + client_secret
        encoded_str = str(base64.b64encode(
            client_string.encode("utf-8")), "utf-8")

        headers = {'Authorization': 'Basic ' + encoded_str}
        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': authorize_uri,
        }
        r = requests.post(
            'https://accounts.spotify.com/api/token', data=data, headers=headers)
        access_token = parse.quote(r.json()['access_token'])
        return access_token

    @staticmethod
    def get_user_info(access_token: str) -> str:
        r = requests.get('https://api.spotify.com/v1/me/',
                         headers={'Authorization': 'Bearer ' + access_token})
        user_id = r.json().get('id', '')
        return user_id
