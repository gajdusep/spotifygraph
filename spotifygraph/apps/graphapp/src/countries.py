"""
This file contains JSON with country information.
The countries JSON is in the following format:
{
    "ABC": {
        "adjectives": [
            "ajective1",
            ...
        ]
        "name": "Abkhazia"
    },
    ...
}
"""

countries_json = {
    "ABC": {
        "adjectives": [
            "abkhaz",
            "abkhazian",
            "abjasio"
        ],
        "name": "Abkhazia"
    },
    "AFG": {
        "adjectives": [
            "afghan",
            "afgano"
        ],
        "name": "Afghanistan"
    },
    "AGO": {
        "adjectives": [
            "angolan",
            "angoleño"
        ],
        "name": "Angola"
    },
    "ALB": {
        "adjectives": [
            "albanian",
            "albanés",
            "albano"
        ],
        "name": "Albania"
    },
    "AND": {
        "adjectives": [
            "andorran",
            "andorrano"
        ],
        "name": "Andorra"
    },
    "ARE": {
        "adjectives": [
            "emirati",
            "emiri",
            "emirian",
            "emiratí"
        ],
        "name": "United Arab Emirates"
    },
    "ARG": {
        "adjectives": [
            "argentinian",
            "argentino"
        ],
        "name": "Argentina"
    },
    "ARM": {
        "adjectives": [
            "armenian",
            "armenio"
        ],
        "name": "Armenia"
    },
    "ATG": {
        "adjectives": [
            "antiguan",
            "barbudan",
            "antiguano"
        ],
        "name": "Antigua and Barbuda"
    },
    "AUS": {
        "adjectives": [
            "australian",
            "australiano"
        ],
        "name": "Australia"
    },
    "AUT": {
        "adjectives": [
            "austrian",
            "austriaco"
        ],
        "name": "Austria"
    },
    "AZE": {
        "adjectives": [
            "azerbaijani",
            "azeri",
            "azerbaiyano"
        ],
        "name": "Azerbaijan"
    },
    "BDI": {
        "adjectives": [
            "burundian",
            "burundés"
        ],
        "name": "Burundi"
    },
    "BEL": {
        "adjectives": [
            "belgian",
            "belga"
        ],
        "name": "Belgium"
    },
    "BEN": {
        "adjectives": [
            "beninese",
            "beninois",
            "beninés"
        ],
        "name": "Benin"
    },
    "BFA": {
        "adjectives": [
            "burkinabé",
            "burkinés"
        ],
        "name": "Burkina Faso"
    },
    "BGD": {
        "adjectives": [
            "bangladeshi",
            "bangladesí"
        ],
        "name": "Bangladesh"
    },
    "BGR": {
        "adjectives": [
            "bulgarian",
            "búlgaro"
        ],
        "name": "Bulgaria"
    },
    "BHR": {
        "adjectives": [
            "bahraini",
            "bareiní"
        ],
        "name": "Bahrain"
    },
    "BHS": {
        "adjectives": [
            "bahamian",
            "bahameño"
        ],
        "name": "The Bahamas"
    },
    "BIH": {
        "adjectives": [
            "bosnian",
            "herzegovinian",
            "bosnio"
        ],
        "name": "Bosnia and Herzegovina"
    },
    "BLR": {
        "adjectives": [
            "belarusian",
            "bielorruso"
        ],
        "name": "Belarus"
    },
    "BLZ": {
        "adjectives": [
            "belizean",
            "beliceño"
        ],
        "name": "Belize"
    },
    "BOL": {
        "adjectives": [
            "bolivian",
            "boliviano"
        ],
        "name": "Bolivia"
    },
    "BRA": {
        "adjectives": [
            "brazilian",
            "brasileño"
        ],
        "name": "Brazil"
    },
    "BRB": {
        "adjectives": [
            "barbadian",
            "barbadense"
        ],
        "name": "Barbados"
    },
    "BRN": {
        "adjectives": [
            "bruneian",
            "bruneano"
        ],
        "name": "Brunei"
    },
    "BTN": {
        "adjectives": [
            "bhutanese",
            "butanés"
        ],
        "name": "Bhutan"
    },
    "BWA": {
        "adjectives": [
            "botswanan",
            "motswana",
            "botsuano"
        ],
        "name": "Botswana"
    },
    "CAF": {
        "adjectives": [
            "central african",
            "centroafricano"
        ],
        "name": "Central African Republic"
    },
    "CAN": {
        "adjectives": [
            "canadian",
            "canadiense"
        ],
        "name": "Canada"
    },
    "CHE": {
        "adjectives": [
            "swiss",
            "suizo"
        ],
        "name": "Switzerland"
    },
    "CHL": {
        "adjectives": [
            "chilean",
            "chileno"
        ],
        "name": "Chile"
    },
    "CHN": {
        "adjectives": [
            "chinese",
            "chino"
        ],
        "name": "China"
    },
    "CIV": {
        "adjectives": [
            "ivorian",
            "marfileño"
        ],
        "name": "Ivory Coast"
    },
    "CMR": {
        "adjectives": [
            "cameroonian",
            "camerunés"
        ],
        "name": "Cameroon"
    },
    "COD": {
        "adjectives": [
            "congolese",
            "congoleño"
        ],
        "name": "Democratic Republic of the Congo"
    },
    "COG": {
        "adjectives": [
            "congolese",
            "congoleño"
        ],
        "name": "Republic of the Congo"
    },
    "COK": {
        "adjectives": [
            "cook island",
            "cookiano"
        ],
        "name": "Cook Islands"
    },
    "COL": {
        "adjectives": [
            "colombian",
            "colombiano"
        ],
        "name": "Colombia"
    },
    "COM": {
        "adjectives": [
            "comoran",
            "comorian",
            "comorano",
            "comorense"
        ],
        "name": "Comoros"
    },
    "CPV": {
        "adjectives": [
            "cabo verdean",
            "caboverdiano"
        ],
        "name": "Cape Verde"
    },
    "CRI": {
        "adjectives": [
            "costa rican",
            "costarricense"
        ],
        "name": "Costa Rica"
    },
    "CUB": {
        "adjectives": [
            "cuban",
            "cubano"
        ],
        "name": "Cuba"
    },
    "CYP": {
        "adjectives": [
            "cypriot",
            "chipriota"
        ],
        "name": "Cyprus"
    },
    "CZE": {
        "adjectives": [
            "czech",
            "checo"
        ],
        "name": "Czech Republic"
    },
    "DEU": {
        "adjectives": [
            "german",
            "alemán"
        ],
        "name": "Germany"
    },
    "DJI": {
        "adjectives": [
            "djiboutian",
            "yibutiano"
        ],
        "name": "Djibouti"
    },
    "DMA": {
        "adjectives": [
            "dominican",
            "dominiqués"
        ],
        "name": "Dominica"
    },
    "DNK": {
        "adjectives": [
            "danish",
            "danés",
            "dinamarqués"
        ],
        "name": "Denmark"
    },
    "DOM": {
        "adjectives": [
            "dominican",
            "dominicano"
        ],
        "name": "Dominican Republic"
    },
    "DZA": {
        "adjectives": [
            "algerian",
            "argelino"
        ],
        "name": "Algeria"
    },
    "ECU": {
        "adjectives": [
            "ecuadorian",
            "ecuatoriano"
        ],
        "name": "Ecuador"
    },
    "EGY": {
        "adjectives": [
            "egyptian",
            "egipcio"
        ],
        "name": "Egypt"
    },
    "ERI": {
        "adjectives": [
            "eritrean",
            "eritreo"
        ],
        "name": "Eritrea"
    },
    "ESH": {
        "adjectives": [
            "sahraouian",
            "sahrawi",
            "sahrawian",
            "saharaui"
        ],
        "name": "Western Sahara"
    },
    "ESP": {
        "adjectives": [
            "spanish",
            "español"
        ],
        "name": "Spain"
    },
    "EST": {
        "adjectives": [
            "estonian",
            "estonio"
        ],
        "name": "Estonia"
    },
    "ETH": {
        "adjectives": [
            "ethiopian",
            "etíope"
        ],
        "name": "Ethiopia"
    },
    "FIN": {
        "adjectives": [
            "finnish",
            "finlandés"
        ],
        "name": "Finland"
    },
    "FJI": {
        "adjectives": [
            "fijian",
            "fiyiano"
        ],
        "name": "Fiji"
    },
    "FRA": {
        "adjectives": [
            "french",
            "francés",
            "francaise"
        ],
        "name": "France"
    },
    "FSM": {
        "adjectives": [
            "micronesian",
            "micronesio"
        ],
        "name": "Micronesia"
    },
    "GAB": {
        "adjectives": [
            "gabonese",
            "gabonés"
        ],
        "name": "Gabon"
    },
    "GBR": {
        "adjectives": [
            "british",
            "english",
            "uk",
            "británico"
        ],
        "name": "United Kingdom"
    },
    "GEO": {
        "adjectives": [
            "georgian",
            "georgiano"
        ],
        "name": "Georgia"
    },
    "GHA": {
        "adjectives": [
            "ghanaian",
            "ghanés"
        ],
        "name": "Ghana"
    },
    "GIN": {
        "adjectives": [
            "guinean",
            "guineano"
        ],
        "name": "Guinea"
    },
    "GMB": {
        "adjectives": [
            "gambian",
            "gambiano"
        ],
        "name": "The Gambia"
    },
    "GNB": {
        "adjectives": [
            "bissau-guinean",
            "bisauguineano",
            "guineano"
        ],
        "name": "Guinea-Bissau"
    },
    "GNQ": {
        "adjectives": [
            "equatoguinean",
            "equatorial guinean",
            "guineano"
        ],
        "name": "Equatorial Guinea"
    },
    "GRC": {
        "adjectives": [
            "greek",
            "hellenic",
            "griego"
        ],
        "name": "Greece"
    },
    "GRD": {
        "adjectives": [
            "grenadian",
            "granadino"
        ],
        "name": "Grenada"
    },
    "GTM": {
        "adjectives": [
            "guatemalan",
            "guatemalteco"
        ],
        "name": "Guatemala"
    },
    "GUY": {
        "adjectives": [
            "guyanese",
            "guyanés"
        ],
        "name": "Guyana"
    },
    "HND": {
        "adjectives": [
            "honduran",
            "hondureño"
        ],
        "name": "Honduras"
    },
    "HRV": {
        "adjectives": [
            "croatian",
            "croata"
        ],
        "name": "Croatia"
    },
    "HTI": {
        "adjectives": [
            "haitian",
            "haitiano"
        ],
        "name": "Haiti"
    },
    "HUN": {
        "adjectives": [
            "hungarian",
            "magyar",
            "húngaro"
        ],
        "name": "Hungary"
    },
    "IDN": {
        "adjectives": [
            "indonesian",
            "indonesio"
        ],
        "name": "Indonesia"
    },
    "IND": {
        "adjectives": [
            "indian",
            "indio"
        ],
        "name": "India"
    },
    "IRL": {
        "adjectives": [
            "irish",
            "irlandés"
        ],
        "name": "Ireland"
    },
    "IRN": {
        "adjectives": [
            "iranian",
            "persian",
            "iraní"
        ],
        "name": "Iran"
    },
    "IRQ": {
        "adjectives": [
            "iraqi",
            "iraquí"
        ],
        "name": "Iraq"
    },
    "ISL": {
        "adjectives": [
            "icelandic",
            "islandés"
        ],
        "name": "Iceland"
    },
    "ISR": {
        "adjectives": [
            "israeli",
            "israelite",
            "israelí"
        ],
        "name": "Israel"
    },
    "ITA": {
        "adjectives": [
            "italian",
            "italiano"
        ],
        "name": "Italy"
    },
    "JAM": {
        "adjectives": [
            "jamaican",
            "jamaicano"
        ],
        "name": "Jamaica"
    },
    "JOR": {
        "adjectives": [
            "jordanian",
            "jordano"
        ],
        "name": "Jordan"
    },
    "JPN": {
        "adjectives": [
            "japanese",
            "japonés"
        ],
        "name": "Japan"
    },
    "KAZ": {
        "adjectives": [
            "kazakh",
            "kazakhstani",
            "kazajo",
            "kazako"
        ],
        "name": "Kazakhstan"
    },
    "KEN": {
        "adjectives": [
            "kenyan",
            "keniano"
        ],
        "name": "Kenya"
    },
    "KGZ": {
        "adjectives": [
            "kirghiz",
            "kirgiz",
            "kyrgyz",
            "kyrgyzstani",
            "kirguíso kirguiso"
        ],
        "name": "Kyrgyzstan"
    },
    "KHM": {
        "adjectives": [
            "cambodian",
            "camboyano"
        ],
        "name": "Cambodia"
    },
    "KIR": {
        "adjectives": [
            "i-kiribati",
            "kiribatiano"
        ],
        "name": "Kiribati"
    },
    "KNA": {
        "adjectives": [
            "kittitian",
            "nevisian",
            "sancristobaleño"
        ],
        "name": "Saint Kitts and Nevis"
    },
    "KOR": {
        "adjectives": [
            "south korean",
            "surcoreano"
        ],
        "name": "South Korea"
    },
    "KWT": {
        "adjectives": [
            "kuwaiti",
            "kuwaití"
        ],
        "name": "Kuwait"
    },
    "LAO": {
        "adjectives": [
            "lao",
            "laotian",
            "laosiano"
        ],
        "name": "Laos"
    },
    "LBN": {
        "adjectives": [
            "lebanese",
            "libanés"
        ],
        "name": "Lebanon"
    },
    "LBR": {
        "adjectives": [
            "liberian",
            "liberiano"
        ],
        "name": "Liberia"
    },
    "LBY": {
        "adjectives": [
            "libyan",
            "libio"
        ],
        "name": "Libya"
    },
    "LCA": {
        "adjectives": [
            "saint lucian",
            "santalucense"
        ],
        "name": "Saint Lucia"
    },
    "LIE": {
        "adjectives": [
            "liechtensteiner",
            "liechtensteiniano"
        ],
        "name": "Liechtenstein"
    },
    "LKA": {
        "adjectives": [
            "sri lankan",
            "ceilanés"
        ],
        "name": "Sri Lanka"
    },
    "LSO": {
        "adjectives": [
            "basotho",
            "lesotense"
        ],
        "name": "Lesotho"
    },
    "LTU": {
        "adjectives": [
            "lithuanian",
            "lituano"
        ],
        "name": "Lithuania"
    },
    "LUX": {
        "adjectives": [
            "luxembourg",
            "luxembourgish",
            "luxemburgués"
        ],
        "name": "Luxembourg"
    },
    "LVA": {
        "adjectives": [
            "latvian",
            "lettish",
            "letón"
        ],
        "name": "Latvia"
    },
    "MAR": {
        "adjectives": [
            "moroccan",
            "marroquí"
        ],
        "name": "Morocco"
    },
    "MCO": {
        "adjectives": [
            "monacan",
            "monégasque",
            "monegasco"
        ],
        "name": "Monaco"
    },
    "MDA": {
        "adjectives": [
            "moldovan",
            "moldavo"
        ],
        "name": "Moldova"
    },
    "MDG": {
        "adjectives": [
            "malagasy",
            "malgache"
        ],
        "name": "Madagascar"
    },
    "MDV": {
        "adjectives": [
            "maldivian",
            "maldivo"
        ],
        "name": "Maldives"
    },
    "MEX": {
        "adjectives": [
            "mexican",
            "mexicano"
        ],
        "name": "Mexico"
    },
    "MHL": {
        "adjectives": [
            "marshallese",
            "marshalés"
        ],
        "name": "Marshall Islands"
    },
    "MKD": {
        "adjectives": [
            "macedonian",
            "macedonio"
        ],
        "name": "North Macedonia"
    },
    "MLI": {
        "adjectives": [
            "malian",
            "malinese",
            "maliense"
        ],
        "name": "Mali"
    },
    "MLT": {
        "adjectives": [
            "maltese",
            "maltés"
        ],
        "name": "Malta"
    },
    "MMR": {
        "adjectives": [
            "burmese",
            "birmano"
        ],
        "name": "Myanmar"
    },
    "MNE": {
        "adjectives": [
            "montenegrin",
            "montenegrino"
        ],
        "name": "Montenegro"
    },
    "MNG": {
        "adjectives": [
            "mongolian",
            "mongol"
        ],
        "name": "Mongolia"
    },
    "MOZ": {
        "adjectives": [
            "mozambican",
            "mozambiqueño"
        ],
        "name": "Mozambique"
    },
    "MRT": {
        "adjectives": [
            "mauritanian",
            "mauritano"
        ],
        "name": "Mauritania"
    },
    "MUS": {
        "adjectives": [
            "mauritian",
            "mauriciano"
        ],
        "name": "Mauritius"
    },
    "MWI": {
        "adjectives": [
            "malawian",
            "malauí"
        ],
        "name": "Malawi"
    },
    "MYS": {
        "adjectives": [
            "malaysian",
            "malasio"
        ],
        "name": "Malaysia"
    },
    "NAM": {
        "adjectives": [
            "namibian",
            "namibio"
        ],
        "name": "Namibia"
    },
    "NER": {
        "adjectives": [
            "nigerien",
            "nigerino"
        ],
        "name": "Niger"
    },
    "NGA": {
        "adjectives": [
            "nigerian",
            "nigeriano"
        ],
        "name": "Nigeria"
    },
    "NIC": {
        "adjectives": [
            "nicaraguan",
            "nicaragüense"
        ],
        "name": "Nicaragua"
    },
    "NIU": {
        "adjectives": [
            "niuean",
            "niueño"
        ],
        "name": "Niue"
    },
    "NLD": {
        "adjectives": [
            "dutch",
            "netherlandic",
            "neerlandés"
        ],
        "name": "Netherlands"
    },
    "NOR": {
        "adjectives": [
            "norwegian",
            "noruego"
        ],
        "name": "Norway"
    },
    "NPL": {
        "adjectives": [
            "nepalese",
            "nepali",
            "nepalés"
        ],
        "name": "Nepal"
    },
    "NRU": {
        "adjectives": [
            "nauruan",
            "nauruano"
        ],
        "name": "Nauru"
    },
    "NZL": {
        "adjectives": [
            "new zealand",
            "new zealandic",
            "neozelandés"
        ],
        "name": "New Zealand"
    },
    "OMN": {
        "adjectives": [
            "omani",
            "omaní"
        ],
        "name": "Oman"
    },
    "PAK": {
        "adjectives": [
            "pakistani",
            "pakistaní"
        ],
        "name": "Pakistan"
    },
    "PAN": {
        "adjectives": [
            "panamanian",
            "panameño"
        ],
        "name": "Panama"
    },
    "PER": {
        "adjectives": [
            "peruvian",
            "peruano"
        ],
        "name": "Peru"
    },
    "PHL": {
        "adjectives": [
            "filipino",
            "philippine",
            "filipino"
        ],
        "name": "Philippines"
    },
    "PLW": {
        "adjectives": [
            "palauan",
            "palauano"
        ],
        "name": "Palau"
    },
    "PNG": {
        "adjectives": [
            "papua new guinean",
            "papuan",
            "papú",
            "papúa"
        ],
        "name": "Papua New Guinea"
    },
    "POL": {
        "adjectives": [
            "polish",
            "polaco"
        ],
        "name": "Poland"
    },
    "PRK": {
        "adjectives": [
            "north korean",
            "norcoreano"
        ],
        "name": "North Korea"
    },
    "PRT": {
        "adjectives": [
            "portuguese",
            "portugués"
        ],
        "name": "Portugal"
    },
    "PRY": {
        "adjectives": [
            "paraguayan",
            "paraguayo"
        ],
        "name": "Paraguay"
    },
    "PSE": {
        "adjectives": [
            "palestinian",
            "palestino"
        ],
        "name": "Palestine"
    },
    "QAT": {
        "adjectives": [
            "qatari",
            "catarí"
        ],
        "name": "Qatar"
    },
    "ROU": {
        "adjectives": [
            "romanian",
            "rumano"
        ],
        "name": "Romania"
    },
    "RUS": {
        "adjectives": [
            "russian",
            "ruso"
        ],
        "name": "Russia"
    },
    "RWA": {
        "adjectives": [
            "rwandan",
            "ruandés"
        ],
        "name": "Rwanda"
    },
    "SAU": {
        "adjectives": [
            "saudi",
            "saudi arabian",
            "saudío saudita"
        ],
        "name": "Saudi Arabia"
    },
    "SDN": {
        "adjectives": [
            "sudanese",
            "sudanés"
        ],
        "name": "Sudan"
    },
    "SEN": {
        "adjectives": [
            "senegalese",
            "senegalés"
        ],
        "name": "Senegal"
    },
    "SGP": {
        "adjectives": [
            "singapore",
            "singaporean",
            "singapurense"
        ],
        "name": "Singapore"
    },
    "SLB": {
        "adjectives": [
            "solomon island",
            "salomonense"
        ],
        "name": "Solomon Islands"
    },
    "SLE": {
        "adjectives": [
            "sierra leonean",
            "sierraleonés"
        ],
        "name": "Sierra Leone"
    },
    "SLV": {
        "adjectives": [
            "salvadoran",
            "salvadoreño"
        ],
        "name": "El Salvador"
    },
    "SMR": {
        "adjectives": [
            "sammarinese",
            "sanmarinense"
        ],
        "name": "San Marino"
    },
    "SOM": {
        "adjectives": [
            "somali",
            "somalí"
        ],
        "name": "Somalia"
    },
    "SOS": {
        "adjectives": [
            "south ossetian",
            "surosetio"
        ],
        "name": "South Ossetia"
    },
    "SRB": {
        "adjectives": [
            "serbian",
            "serbio"
        ],
        "name": "Serbia"
    },
    "SSD": {
        "adjectives": [
            "south sudanese",
            "sursudanés"
        ],
        "name": "South Sudan"
    },
    "STP": {
        "adjectives": [
            "são toméan",
            "santotomense"
        ],
        "name": "São Tomé and Príncipe"
    },
    "SUR": {
        "adjectives": [
            "surinamese",
            "surinamés"
        ],
        "name": "Suriname"
    },
    "SVK": {
        "adjectives": [
            "slovak",
            "eslovaco",
            "czsk"
        ],
        "name": "Slovakia"
    },
    "SVN": {
        "adjectives": [
            "slovene",
            "slovenian",
            "esloveno"
        ],
        "name": "Slovenia"
    },
    "SWE": {
        "adjectives": [
            "swedish",
            "sueco"
        ],
        "name": "Sweden"
    },
    "SWZ": {
        "adjectives": [
            "swati",
            "swazi",
            "suazi"
        ],
        "name": "Swaziland"
    },
    "SYC": {
        "adjectives": [
            "seychellois",
            "seychellense"
        ],
        "name": "Seychelles"
    },
    "SYR": {
        "adjectives": [
            "syrian",
            "sirio"
        ],
        "name": "Syria"
    },
    "TCD": {
        "adjectives": [
            "chadian",
            "chadiano"
        ],
        "name": "Chad"
    },
    "TGO": {
        "adjectives": [
            "togolese",
            "togolés"
        ],
        "name": "Togo"
    },
    "THA": {
        "adjectives": [
            "thai",
            "tailandés"
        ],
        "name": "Thailand"
    },
    "TJK": {
        "adjectives": [
            "tajikistani",
            "tayiko"
        ],
        "name": "Tajikistan"
    },
    "TKM": {
        "adjectives": [
            "turkmen",
            "turcomano"
        ],
        "name": "Turkmenistan"
    },
    "TLS": {
        "adjectives": [
            "timorese",
            "timorense"
        ],
        "name": "East Timor"
    },
    "TON": {
        "adjectives": [
            "tongan",
            "tongano"
        ],
        "name": "Tonga"
    },
    "TTO": {
        "adjectives": [
            "tobagonian",
            "trinidadian",
            "trinitense"
        ],
        "name": "Trinidad and Tobago"
    },
    "TUN": {
        "adjectives": [
            "tunisian",
            "tunecino"
        ],
        "name": "Tunisia"
    },
    "TUR": {
        "adjectives": [
            "turkish",
            "turco"
        ],
        "name": "Turkey"
    },
    "TUV": {
        "adjectives": [
            "tuvaluan",
            "tuvaluano"
        ],
        "name": "Tuvalu"
    },
    "TWN": {
        "adjectives": [
            "formosan",
            "taiwanese",
            "taiwanés"
        ],
        "name": "Taiwan"
    },
    "TZA": {
        "adjectives": [
            "tanzanian",
            "tanzano"
        ],
        "name": "Tanzania"
    },
    "UGA": {
        "adjectives": [
            "ugandan",
            "ugandés"
        ],
        "name": "Uganda"
    },
    "UKR": {
        "adjectives": [
            "ukrainian",
            "ucraniano"
        ],
        "name": "Ukraine"
    },
    "URY": {
        "adjectives": [
            "uruguayan",
            "uruguayo"
        ],
        "name": "Uruguay"
    },
    "USA": {
        "adjectives": [
            "american",
            "u.s.",
            "united states",
            "americano",
            "estadounidense",
            "norteamericano"
        ],
        "name": "United States"
    },
    "UZB": {
        "adjectives": [
            "uzbek",
            "uzbekistani",
            "uzbeko"
        ],
        "name": "Uzbekistan"
    },
    "VAT": {
        "adjectives": [
            "vatican",
            "vaticano"
        ],
        "name": "Vatican City"
    },
    "VCT": {
        "adjectives": [
            "saint vincentian",
            "vincentian",
            "sanvicentino"
        ],
        "name": "Saint Vincent and the Grenadines"
    },
    "VEN": {
        "adjectives": [
            "venezuelan",
            "venezolano"
        ],
        "name": "Venezuela"
    },
    "VNM": {
        "adjectives": [
            "vietnamese",
            "vietnamita"
        ],
        "name": "Vietnam"
    },
    "VUT": {
        "adjectives": [
            "ni-vanuatu",
            "vanuatuan",
            "vanuatuense"
        ],
        "name": "Vanuatu"
    },
    "WSM": {
        "adjectives": [
            "samoan",
            "samoano"
        ],
        "name": "Samoa"
    },
    "XXK": {
        "adjectives": [
            "kosovan",
            "kosovar",
            "kosovar"
        ],
        "name": "Kosovo"
    },
    "YEM": {
        "adjectives": [
            "yemeni",
            "yemení"
        ],
        "name": "Yemen"
    },
    "ZAF": {
        "adjectives": [
            "south african",
            "sudafricano"
        ],
        "name": "South Africa"
    },
    "ZMB": {
        "adjectives": [
            "zambian",
            "zambiano"
        ],
        "name": "Zambia"
    },
    "ZWE": {
        "adjectives": [
            "zimbabwean",
            "zimbabuense"
        ],
        "name": "Zimbabwe"
    }
}
