from typing import List, Set, Tuple, Dict, Union
import networkx as nx
import community
from sklearn.cluster import SpectralClustering

from apps.graphapp.src.models import Song, Artist, ArtistEdge, ClusterEdge, ClusterNode, Position
from apps.graphapp.src.genre_diversifier import GenreDiversifier
from apps.graphapp.src.graph_visualizer import GraphVisualizer
from apps.graphapp.src.clusterizer import Clusterizer


class Graph:
    """
    A wrapper with all graph methods.
    """

    def __init__(self, nodes: List[Artist], edges: List[ArtistEdge]):
        self.nodes = nodes
        self.edges = edges

        # create artists dict
        self.artists_dict: Dict[str, Artist] = {}
        for a in nodes:
            self.artists_dict[a.id] = a

        # create the graph
        nx_edges = [(x.id1, x.id2, {'weight': x.weight}) for x in edges]
        self.g = nx.Graph()
        self.g.add_nodes_from([x.id for x in nodes])
        self.g.add_edges_from(nx_edges)

        # set the diversifier
        self.genre_diversifier = GenreDiversifier(self.artists_dict)

        # visualisation parameters
        self.graph_width = 1200
        self.node_width = 500

        # visualization objects
        self.cluster_graph_visualizer = GraphVisualizer(self.graph_width, self.node_width)
        # self.inside_cluster_graph_visualizer = GraphVisualizer(self.graph_width, self.node_width / 2)
        self.inside_cluster_graph_visualizer = GraphVisualizer(self.graph_width, 250)

        # clustering parameters
        self.min_artists_in_cluster = 5
        # self.min_artists_in_cluster = 5

        self.max_artists_in_cluster = max(15, len(nodes) // 10)
        # self.max_artists_in_cluster = 30
        print('max',self.max_artists_in_cluster)

        self.min_artists_in_component_to_do_clustering = 20
        # self.min_artists_in_component_to_do_clustering = 20
        self.clusterizer = Clusterizer(
            self.min_artists_in_cluster, self.max_artists_in_cluster, self.min_artists_in_component_to_do_clustering)

    def process(self):
        print('---Processing graph---')

        # get the connected components indexes
        connected_components: List[Set[str]] = sorted(
            nx.connected_components(self.g), key=len, reverse=True)

        # process every connected component - subdivide into clusters
        cluster_nodes, induced_graph_edges, positions = self.process_connected_components(
            connected_components)

        # process isolated
        isolates = list(nx.isolates(self.g))
        if len(isolates) > 0:
            cluster_nodes.append(ClusterNode(id=1000, genres=['TOTAL MIX'],
                artists_positions_dict=dict(zip(isolates, [[]]*len(isolates)))))
            positions.append({"1000": Position(0, 0)})

        for cluster in cluster_nodes:
            inside_cluster_postions = self.show_inside_cluster(cluster, self.g)
            for key in cluster.artists_positions_dict.keys():
                cluster.artists_positions_dict[key] = inside_cluster_postions[key]

        # self.cluster_graph_visualizer.visualize_clusters_graph(induced_graph_nodes, induced_graph_edges)

        # print the graph into the file
        nodes_jsons = [x.to_json() for x in cluster_nodes]
        edges_jsons = [x.to_json() for x in induced_graph_edges]
        cluster_squares_jsons = []
        for square in positions:
            current_square = {}
            for c_key, c_value in square.items():
                current_square[c_key] = {'x': c_value.x, 'y': c_value.y}
            cluster_squares_jsons.append(current_square)

        induced_graph_json = {
            'nodes': nodes_jsons,
            'edges': edges_jsons,
            'positions': cluster_squares_jsons
        }

        return induced_graph_json

    def show_inside_cluster(self, cluster: ClusterNode, graph):
        sg = nx.subgraph(graph, cluster.artists_positions_dict)
        _, inside_cluster_postions = self.inside_cluster_graph_visualizer.visualize(
            sg)
        return inside_cluster_postions

    def process_connected_components(self, connected_components: List[Set[str]]) \
        -> Tuple[List[ClusterNode], List[ClusterEdge], List[Dict[Union[str, int], Position]]]:

        min_clusters_in_component = 3

        current_id = 0  # cluster id to create the induced graph
        induced_graph_nodes = []
        induced_graph_edges = []
        positions = []

        small_components_induced_graph_nodes = []
        small_components_induced_graph_edges = []
        small_components_positions = []

        for component in connected_components:
            if len(component) <= 1:  # component contains only one node, will be added to the isolates
                continue

            component_graph = self.g.subgraph(component)
            cluster_nodes, intercluster_edges = self.process_component(
                component_graph, current_id)

            if len(cluster_nodes) > min_clusters_in_component:
                induced_graph_nodes.extend(cluster_nodes)
                induced_graph_edges.extend(intercluster_edges)
            else:
                _, positions_in_cluster = self.cluster_graph_visualizer.visualize_clusters_graph(
                    cluster_nodes, intercluster_edges)

                small_components_positions.append(positions_in_cluster)
                small_components_induced_graph_nodes.extend(cluster_nodes)
                small_components_induced_graph_edges.extend(intercluster_edges)

            current_id += len(cluster_nodes)

        complete_graph, complete_graph_positions = self.cluster_graph_visualizer.visualize_clusters_graph(
            induced_graph_nodes, induced_graph_edges)

        if len(induced_graph_nodes) == 0:
            return (
                small_components_induced_graph_nodes,
                small_components_induced_graph_edges,
                small_components_positions
            )
        elif len(small_components_induced_graph_nodes) == 0:
            return (
                induced_graph_nodes,
                induced_graph_edges,
                [complete_graph_positions]
            )

        return (
            induced_graph_nodes + small_components_induced_graph_nodes,
            induced_graph_edges + small_components_induced_graph_edges,
            [complete_graph_positions] + small_components_positions
        )

    def process_component(self, component_graph, id_num) -> Tuple[List[ClusterNode], List[ClusterEdge]]:
        number_of_nodes = len(component_graph.nodes)

        partition = self.clusterizer.clusterize(component_graph)
        induced_graph = community.induced_graph(partition, component_graph)

        clusters: List[ClusterNode] = []
        induced_graph_edges: List[ClusterEdge] = []

        # induced graph -> made from clusters single nodes
        for i in range(len(induced_graph.nodes())):
            clusters.append(
                ClusterNode(id=(id_num+i), genres=[],
                            artists_positions_dict={})
            )

        # put the nodes into the clusters
        for artist_id, cluster_id in partition.items():
            clusters[cluster_id].artists_positions_dict[artist_id] = Position(0, 0)

        # put the edges between the induced clusters
        for edge in induced_graph.edges():
            induced_graph_edges.append(
                ClusterEdge(
                    cluster_1=edge[0] + id_num, cluster_2=edge[1] + id_num,
                    weight=round(induced_graph[edge[0]][edge[1]]['weight'], 1)
                )
            )

        for cluster in clusters:
            top_genres, country = self.get_common_genres(
                list(cluster.artists_positions_dict.keys()))
            cluster.genres = top_genres
            cluster.country = country

        return clusters, induced_graph_edges

    def get_common_genres(self, artist_ids: List[str], top_n=5):
        return self.genre_diversifier.process(artist_ids, top_n)
