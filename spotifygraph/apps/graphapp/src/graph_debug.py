from typing import List
from collections import Counter
import json
import os
import time
from apps.graphapp.src.models import Artist, ArtistEdge
from apps.graphapp.src.graph import Graph
from apps.graphapp.src.edges_creator import EdgesCreator

file_path = '../files/TOFILL.json'


def current_milli_time():
    return int(round(time.time() * 1000))


def get_path_file(relative_path: str):
    SCRIPT_DIR = os.path.dirname(__file__)
    filetest = relative_path
    pathfile = os.path.join(SCRIPT_DIR, filetest)
    return pathfile


def write_all_to_one_file(json_to_write):
    with open(get_path_file(file_path), 'w') as filetowrite:
        json.dump(json_to_write, fp=filetowrite, indent=4)


def read_all_from_one_file():
    with open(get_path_file(file_path)) as json_file:
        data = json.load(json_file)
    return data


def parse_nodes(data) -> List[Artist]:
    nodes = []
    for _, d in data.items():
        nodes.append(Artist(
            id=d['id'], name=d['name'], songs=d['likedSongs'], genres=d['genres'],
            popularity=d['score'], image_link=d['image'], related_artists_ids=d['relatedArtistsIds']
        ))
    return nodes


def add_cluster_artists_ids(clusters):
    for cluster in clusters:
        cluster['artistsIds'] = []
    return clusters    

def process_graph():
    old_json = read_all_from_one_file()

    start = time.time()

    nodes_json = old_json['artistDict']
    nodes = parse_nodes(nodes_json)
    new_nodes_json = {}
    for node in nodes:
        new_nodes_json[node.id] = node.to_json()

    artist_dict = {}
    for n in nodes:
        artist_dict[n.id] = n

    edges_creator = EdgesCreator()
    artist_edges = edges_creator.get_edges(artist_dict)
    new_edges_json = list(map(lambda x: x.to_json(), artist_edges))

    my_graph = Graph(nodes, artist_edges)

    graph_json = my_graph.process()
    end = time.time()
    print('time spent:', end - start)

    json_to_return = {}
    json_to_return['artistDict'] = new_nodes_json
    json_to_return['artistEdges'] = new_edges_json
    json_to_return['clusters'] = add_cluster_artists_ids(graph_json['nodes'])
    json_to_return['clusterEdges'] = graph_json['edges']
    json_to_return['clusterSquares'] = graph_json['positions']

    write_all_to_one_file(json_to_return)


def create_country_adjectives():
    """
    Helping method, not needed anymore...
    """
    countries = {}

    with open(get_path_file('../files/en.json'), 'r') as en:
        with open(get_path_file('../files/es.json'), 'r') as es:
            data_en = json.load(en)
            data_es = json.load(es)

            for key in data_en['countries'].keys():

                en_obj = data_en['countries'][key]
                es_obj = data_es['countries'][key]

                en_adj = [s.lower() for s in en_obj.get('adjectives', [])]
                es_adj = [s.lower() for s in es_obj.get('adjectives', [])]
                total_adj = [*en_adj, *es_adj]

                countries[key] = {
                    'adjectives': total_adj,
                    'name': en_obj['name']
                }

    with open(get_path_file('../files/countries.json'), 'w') as co:
        co.writelines(json.dumps(countries, indent=4,
                                 ensure_ascii=False).encode('utf8').decode())


def filter_out_country_adjectives():

    adjectives = []
    genres = []
    filtered_genres = set([])

    with open(get_path_file('../files/countries.json'), 'r') as countries:
        countries_json = json.load(countries)
        for key in countries_json.keys():
            adjectives.extend(countries_json[key]['adjectives'])

    with open(get_path_file('../files/genres_all.json'), 'r') as countries:
        genres = json.load(countries)

    print(len(adjectives))

    for genre in genres:
        genre_split = genre.split(' ')
        if len(genre_split) > 1:
            new_genre_split = []
            for s in genre_split:
                if s not in adjectives:
                    new_genre_split.append(s)
            new_genre_name = ' '.join(new_genre_split)
        else:
            new_genre_name = genre
        filtered_genres.add(new_genre_name)

    print(len(genres))
    print(len(filtered_genres))

    # check the number of single words
    filtered_genres = list(filtered_genres)
    all_list = []
    for fg in filtered_genres:
        all_list.extend(fg.split(' '))

    c = Counter(all_list).most_common()
    print(c[:10])
    print(len(c))


if __name__ == "__main__":
    process_graph()
    # filter_out_country_adjectives()
