import networkx as nx
import community
from typing import List, Set, Tuple, Dict
from sklearn.cluster import SpectralClustering
from collections import deque


class Clusterizer:
    """
    Class for graph clustering. Initialize the `Clusterizer` object with
    cluster size parameters and then call the `clusterize` method.
    """

    def __init__(self, min_artists_in_cluster: int, max_artists_in_cluster: int,
                 min_artists_in_component_to_do_clustering: int):
        """
        min_artists_in_cluster: minimal number of artists in the generated clusters\\
        max_artists_in_cluster: minimal number of artists in the generated clusters\\
        min_artists_in_component_to_do_clustering: minimal number of artists in the given component.
            If the size of the component is smaller, the clustering doesn't start at all
        """
        self.min_artists_in_cluster = min_artists_in_cluster
        self.max_artists_in_cluster = max_artists_in_cluster
        self.min_artists_in_component_to_do_clustering = min_artists_in_component_to_do_clustering

    def clusterize(self, component_graph: nx.Graph) -> Dict[str, int]:
        """
        Calculates graph clustering, using `spectral_clustering` by `sklearn.cluster`

        component_graph: nx.Graph we want to clusterize.\\
        returns: Dict[str,int]: { 'artistID':cluster_id }
        """

        # if there is only a small number of artists in the component, don't clusterize it at all
        if len(component_graph.nodes()) < self.min_artists_in_component_to_do_clustering:
            return {node: 0 for node in component_graph.nodes()}

        initial_clustering = self.spectral_clustering(
            component_graph, n_clusters=3)
        initial_clusters = self.from_artist_dict(initial_clustering)

        # until there are some clusters with number of artists bigger than maximal capacity, split the clusters
        result_clusters: List[List[str]] = []
        not_processed_clusters_queue = deque(initial_clusters.values())
        while len(not_processed_clusters_queue) > 0:
            current_cluster = not_processed_clusters_queue.popleft()

            if len(current_cluster) > self.max_artists_in_cluster:
                subgraph = nx.subgraph(component_graph, current_cluster)
                new_clustering = self.spectral_clustering(
                    subgraph, n_clusters=3)
                new_clusters = self.from_artist_dict(new_clustering).values()
                for nc in new_clusters:
                    not_processed_clusters_queue.append(nc)
            else:
                result_clusters.append(current_cluster)

        # for all of the calculated clusters, give them the cluster_id
        result = {}
        for i in range(len(result_clusters)):
            for j in result_clusters[i]:
                result[j] = i

        # remove all clusters that are smaller than minimal capacity
        self.remove_small_clusters(result, component_graph)
        result = self.from_cluster_id_dict(
            self.normalize_clusters_ids(self.from_artist_dict(result)))

        return result

    def remove_small_clusters(self, result: Dict[str, int], component_graph: nx.Graph):
        cl = self.from_artist_dict(result)
        small_clusters_ids = []

        # get all clusters that have too few artists
        for cluster_id, artist_arr in cl.items():
            if len(artist_arr) < self.min_artists_in_cluster:
                small_clusters_ids.append(cluster_id)

        # if there is no too small cluster, just return
        if len(small_clusters_ids) == 0:
            return

        # create induced graph to find quickly the edges
        induced_graph = community.induced_graph(result, component_graph)

        # for every small cluster, find the most similar cluster and add the nodes into it
        translation_dict = {}
        for small_cluster_id in small_clusters_ids:
            edges_with_cluster = induced_graph.edges([small_cluster_id])

            if len(edges_with_cluster) < 1:
                continue

            max_weight = 0
            second_cluster_id = small_cluster_id
            for edge in edges_with_cluster:
                if edge[0] == edge[1]:
                    continue
                second_id = edge[1] if edge[0] == small_cluster_id else edge[0]
                
                weight = round(induced_graph[small_cluster_id][second_id]['weight'], 1)
                if weight > max_weight:
                    max_weight = weight
                    second_cluster_id = second_id

            translation_dict[small_cluster_id] = second_cluster_id
                        
        # propagate the translation through the dict
        for c_id_from, c_id_to in translation_dict.items():
            curr_to = c_id_to
            while curr_to in translation_dict:
                if translation_dict[curr_to] == c_id_from:
                    break
                
                curr_to = translation_dict[curr_to]
            translation_dict[c_id_from] = curr_to
        
        # translate the clusters ids
        for c_id_from, c_id_to in translation_dict.items():
            for artist_id in cl[c_id_from]:
                result[artist_id] = c_id_to                

    def normalize_clusters_ids(self, artists_dict: Dict[int, List[str]]) -> Dict[int, List[str]]:
        """
        Recomputes cluster ids to begin from 0 and do continue with 1,2,3...
        """
        i = 0
        normalized = {}
        for k, v in artists_dict.items():
            normalized[i] = v
            i += 1
        return normalized

    def spectral_clustering(self, g: nx.Graph, n_clusters) -> Dict[str, int]:
        """
        returns: Dict[artist_id, label_of_cluster]
        """
        if n_clusters > len(g.nodes()):
            n_clusters = 1
        adj_mat = nx.to_numpy_matrix(g)
        sc = SpectralClustering(affinity="precomputed", n_clusters=n_clusters)
        sc.fit(adj_mat)

        node_names = g.nodes()
        dict_to_ret = dict(zip(node_names, sc.labels_))

        return dict_to_ret

    def from_artist_dict(self, artist_cluster: Dict[str, int]) -> Dict[int, List[str]]:
        cluster_artists: Dict[int, List[str]] = {}
        for key, value in artist_cluster.items():
            if value in cluster_artists:
                cluster_artists[value].append(key)
            else:
                cluster_artists[value] = [key]
        return cluster_artists

    def from_cluster_id_dict(self, cluster_artists: Dict[int, List[str]]) -> Dict[str, int]:
        artist_cluster = {}
        for cluster_id, artists in cluster_artists.items():
            for ar in artists:
                artist_cluster[ar] = cluster_id
        return artist_cluster

    def partition_dict_to_list(self, partition_dict):
        pass

    def dendrogram(self, graph: nx.Graph):
        dendro = community.generate_dendrogram(graph)
        return dendro
