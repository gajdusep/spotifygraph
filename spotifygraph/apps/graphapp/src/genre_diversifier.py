from typing import List, Tuple, Union, Dict, Set
from collections import Counter
from apps.graphapp.src.models import Artist
from apps.graphapp.src.countries import countries_json


class GenreDiversifier():
    """
    The wrapper for calculation of the cluster description.\\
    Initialize the `GenreDiversifier` object and call `process` on it.
    """

    def __init__(self, artists_dict: Dict[str, Artist]):
        self.artists_dict: Dict[str, Artist] = artists_dict
        self.countries_json = countries_json
        self.code_country_dict = self.get_code_country_dict(
            self.countries_json)
        self.country_adjectives = self.get_country_adjectives(
            self.countries_json)
        self.minimal_country_occurance = 0.4

    def process(self, artist_ids: List[str], top_n: int = 5) -> Tuple[List[str], str]:
        """
        artist_ids: list of ids of artists in a certain cluster\\
        top_n: how many genres maximally should be returned. \\
        returns: (list of top_n genres, country code - empty str if no country was found)
        """

        genre_list = []
        countries_list = []

        # for each artist get the country if any, and new genres
        for artist_id in artist_ids:
            the_artist = self.artists_dict.get(artist_id, None)
            if the_artist is not None and the_artist.genres:
                filtered_genres, countries = self.process_artists_genres(
                    the_artist.genres)
                genre_list.extend(filtered_genres)
                countries_list.extend(countries)

        genre_counter = Counter(genre_list).most_common()
        country_counter = Counter(countries_list).most_common()

        country_code = ''
        if country_counter and country_counter[0][1] / len(artist_ids) > self.minimal_country_occurance:
            country_code = self.code_country_dict[country_counter[0][0]]

        top_genres = self.filter_duplicate_genres(genre_counter, top_n)

        return top_genres, country_code

    def get_country_adjectives(self, countries_json) -> List[str]:
        adjectives: List[str] = []
        for key in countries_json.keys():
            adjectives.extend(countries_json[key]['adjectives'])
        return adjectives

    def get_code_country_dict(self, countries_json) -> Dict[str, str]:
        code_country_dict = {}
        for key in countries_json.keys():
            for adj in countries_json[key]['adjectives']:
                code_country_dict[adj] = key
        return code_country_dict

    def filter_out_country_adjective(self, genre: str) -> Tuple[str, Union[str, None]]:
        """
        IMPORTANT:
        - for simplicity: suppose in every genre, only 1 country adjective can be present!

        returns: (string without country adjective, country_adjective)
            country_adjective is None if none adjective is found
        """

        genre_split = genre.split(' ')
        country_adjective = None

        if len(genre_split) < 2:
            return genre, country_adjective

        found_1_word = False
        found_2_word = False

        new_genre_name = genre
        # one word country names
        new_genre_split_words = []
        for i in range(len(genre_split)):
            if genre_split[i] not in self.country_adjectives:
                new_genre_split_words.append(genre_split[i])
            else:
                found_1_word = True
                country_adjective = genre_split[i]

        if not found_1_word:
            # two words country names
            new_genre_split_words = []
            for i in range(0, len(genre_split)-1):
                potential_counry_adj = genre_split[i-1] + ' ' + genre_split[i]
                if potential_counry_adj in self.country_adjectives:
                    found_2_word = True
                    country_adjective = potential_counry_adj
                    new_genre_split_words = genre_split[0:i - 1] + genre_split[i+1:len(genre_split)]
                    new_genre_name = ' '.join(new_genre_split_words)
                    break
        else:
            new_genre_name = ' '.join(new_genre_split_words)

        return new_genre_name, country_adjective

    def process_artists_genres(self, genres) -> Tuple[List[str], List[str]]:
        """
        genres: list of original spotify genres
        returns: (new genre names without country, list of countries of origin - expected maximaly 1)
        """
        countries = set([])
        new_genres = []
        for genre in genres:
            filtered_genre, country = self.filter_out_country_adjective(genre)
            if country:
                countries.add(country)
            new_genres.append(filtered_genre)

        return new_genres, list(countries)

    def filter_duplicate_genres(self, genre_counter, top_n) -> List[str]:
        top_genres = []
        already_obtained_words: Set[str] = set([])
        already_obtained_words_list: List[str] = []
        for i in range(min(top_n, len(genre_counter))):
            g = genre_counter[i][0]
            g_split = g.replace('-', ' ').split(' ')

            word_counter = Counter(already_obtained_words_list)
            to_add = True
            if len(set(g_split).intersection(already_obtained_words)) / len(g_split) >= 0.6:
                to_add = False
            else:
                for g_split_w in g_split:
                    if word_counter[g_split_w] > 1:
                        to_add = False

            if to_add:
                top_genres.append(g)
                already_obtained_words = already_obtained_words.union(g_split)
                already_obtained_words_list.extend(g_split)

        return top_genres
