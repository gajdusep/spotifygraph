# spotifygraph

This repository contains code of the bachelor thesis, written by Pavel Gajdusek.

Visit [spotifygraph homepage](https://gajdusep.github.io/spotifygraph/)

## Overview

This web application shows the spotify user's clusterized data in the form of a graph. 

The graph is based on:

1) provided data by Spotify - related artists
2) similarity based on intersection of genres

Backend is written in `Python`, frontend in `React`.

## Loading graph examples

If you don't have many artists in your Spotify account and/or want to see more graphs,
choose one of the files in the `testfiles` folder and upload it 
by clicking the upload button in the left side-bar.

## Examples

### NEW: ADDED NEW ARTISTS RECOMMENDATION

![Recommendation](./images/example3.png)

![Example](./images/example.png)

![Example](./images/example2.png)

## frontend

Debugging:

```bash
cd frontend/
npm install
npm start
```

## backend

Debugging:

```bash
cd spotifygraph/
pip3 install -r requirements.txt
python3 manage.py runserver
```

It is necessary to set `client_id` and `client_secret` to env variables:

```bash
export SPOTIFY_CLIENT_ID=...
export SPOTIFY_CLIENT_SECRET=...
export SPOTIFY_GRAPH_AUTHORIZE_URI=...
```

The `client_secret` variable is secret, for further information, please contact the developer.

## Backend code structure

Python source files and all following files are located in the `spotifygraph/apps/graphapp` folder.

`urls.py` and `views.py` contain endpoints information.

`src/graph.py` contains wrapper around graph calculation. 
Other files in `src` folder contain classes that are wrappers around certain graph algorithms, e.g.
`clusterizer.py`, `graph_visualizer.py`, `genre_diversifier.py` etc. 

Model classes are located in the `src/models.py` file.

## Frontend code structure

The `frontend/src/components` folder contains components files and css files. 

The `frontend/src/methods` folder contains files with methods that are not strictly depending on the components.

The `frontend/src/store` folder contains `actions.tsx`, `reducers.tsx` (both containing methods using redux store) 
and `types.tsx` (data classes used by code in all files).
